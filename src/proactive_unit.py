import time, sys
import conf_manager as cm
import enforcement_unit as eu
import sec_assess_unit as sau
import conf_generator as cg
import att_hist_collector as ahc
from threading import Thread
from queue import Queue

yellow = lambda text: '\033[0;33m' + text + '\033[0m'
bold = lambda text: '\033[0;1m' + text + '\033[0m'

att_alerted = Queue()

class ProactiveUnit(Thread):

    def __init__(self, conf_m, enf_u, sl, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cm = conf_m
        self.sec_l = sl
        self.eu = enf_u
        self.am_unit = ahc.AlertManager(self.cm)
        self.det_queue = att_alerted
        self.alive = True
        self.sec_unit = sau.SecurityAssessmentUnit()
        self.sec_unit.activeConf = self.cm.activeConf
            

    def run(self):
        iterations = 0
            
        # while (iterations < 551) :
        while self.alive :
            print(bold(f'^^^^^^^^^^^^^^^^^ ITERATION N.{int(iterations / 10) + 1} ^^^^^^^^^^^^^^^^^'))
            t_cm = Thread(target=self.cm.run_proactiveStage, args=(self.sec_l, self.eu, self.det_queue))
            t_cm.start()
            t_sa = Thread(target=self.sec_unit.ps_secAssessment)
            t_sa.start()
            
            t_cm.join()
            t_sa.join()
            
            # time.sleep(1)
            # self.cm.print_active()
            # iterations += 10
            # sys.exit(0)
        
        print(yellow("----------------- PROACTIVE UNIT STOPPED -----------------"))
    

    def stop(self):
        self.sec_unit.stop()
        self.cm.stop()
        self.am_unit.stop()
        self.alive = False

