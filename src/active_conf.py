import re

activeConf = None


# APPLICATION DEPLOYMENT COMPONENT
# Represents how a component (DB, WebApp, Service) is deployed into cloud
# application. If it is a VM, it will be provided by a CSP. If it is a 
# software tool or platform which hosts a service (i.e., a DBMS for a 
# database), a VM hosts.
class AppDeploymentComponent(object):

    def __init__(self, i, t, n, v, p, ve):
        self.id = i
        self.type = t
        self.name = n
        self.vendor = v
        self.product = p
        self.version = ve
        self.ip_addr = None
        self.vm_type = None
        self.host = None
        self.provider = None


# APPLICATION COMPONENT
# Represents a component (DB, WebApp, Service) of cloud application.
class ApplicationComponent(object):

    def __init__(self, id_a, type_a, id_adc):
        self.id = id_a
        self.type = type_a
        self.host = id_adc
        self.vm = None


# ACTIVE APPLICATION DEPLOYMENT CONFIGURATION
# Describes currently active application deployment configuration
# to enforce into cloud and to monitor for security reasons.
class ActiveConfiguration(object):
    #__basepath = './docs/'

    def __init__(self, data, idx):
        self.id = idx
        # read JSON files of application deployment configurations
        # with open('./docs/conf_{}.json'.format(idx)) as f:
        #     data = json.load(f)
        self.num_dcomponents = data['num_dcomponents']

        # generate application components
        self.components = []
        for i in range(len(data['components'][0]['nodes'])):
            id_ac = data['components'][0]['nodes'][i]['id_node']
            type_ac = data['components'][0]['nodes'][i]['type']
            host_ac = data['components'][0]['nodes'][i]['host']
            ac = ApplicationComponent(id_ac, type_ac, host_ac)
            if re.match("(VM\w+)", type_ac):
                vm_ac = data['components'][0]['nodes'][i]['vm']
                ac.vm = vm_ac
            self.components.append(ac)
            # print("AC added")
        # generate application deployment components
        self.dcomponents = []
        for i in range(self.num_dcomponents):
            id_adc = data['dcomponents'][0]['nodes'][i]['id_comp']
            type_adc = data['dcomponents'][0]['nodes'][i]['type']
            name_adc = data['dcomponents'][0]['nodes'][i]['name']
            vendor_adc = data['dcomponents'][0]['nodes'][i]['vendor']
            product_adc = data['dcomponents'][0]['nodes'][i]['product']
            version_adc = data['dcomponents'][0]['nodes'][i]['version']
            adc = AppDeploymentComponent(id_adc, type_adc, name_adc, vendor_adc, product_adc, version_adc)
            if re.match("(VM\w+)", type_adc):
                vmt_adc = data['dcomponents'][0]['nodes'][i]['vm_type']
                provider_adc = data['dcomponents'][0]['nodes'][i]['provider']
                adc.vm_type = vmt_adc
                adc.provider = provider_adc
            elif re.match("\W(CSP\w+)", type_adc):
                host_adc = data['dcomponents'][0]['nodes'][i]['host']
                adc.host = host_adc
            self.dcomponents.append(adc)
            # print("ADC added")


def set_activeConf(data, idx):
    global activeConf
    activeConf = ActiveConfiguration(data, idx)


def set_ip(idx, ip):
    global activeConf
    activeConf.dcomponents[idx].ip_addr = ip