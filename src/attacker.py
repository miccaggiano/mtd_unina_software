import time, sys, json, random
import matplotlib.pyplot as plt
import seaborn as sns
import pickle as pk
import pandas as pd
import numpy as np
import arbiter
from threading import Thread
from att_hist_collector import AlertManager

red = lambda text: '\033[0;31m' + text + '\033[0m'
magenta = lambda text: '\033[0;35m' + text + '\033[0m'
bold = lambda text: '\033[0;1m' + text + '\033[0m'
cyan = lambda text: '\033[0;36m' + text + '\033[0m'
pink = lambda text: '\033[0;95m' + text + '\033[0m'

ATTACKERS_EXPERTISE = {"LOW": 0.75, "HIGH": 0.35}
ATTACKER_SKILLS = None
attack_durations = {
    "scan": 5, "ddos": 12, "csrf": 6, "redirect": 6, "xss": 8, "sql_injection": 7,
    "os_injection": 8, "trojan": 7, "worm": 8, "rootkit": 9, "buffer_overflow": 7, 
    "jamming": 10, "side_channel": 10
}
attack_types = [
    "ddos", "csrf", "redirect", "xss", "sql_injection", "os_injection", 
    "trojan", "worm", "rootkit", "buffer_overflow", "jamming", "side_channel"
]

comp_types = ["DBMS", "WS", "VM"]
os_types = ["enterprise_linux", "windows_server_2016", "freebsd", "solaris"]
# os_types = ["ubuntu_linux", "enterprise_linux", "windows_server_2016", "freebsd", "solaris"]
dbms_types = ["sqlite", "postgresql", "mariadb"]
# dbms_types = ["mysql", "postgresql", "mariadb"]
ws_types = ["tomcat", "nginx", "internet_information_server"]


cve_targets = {
    "CVE-2018-20505": {
        "type" : "sql_injection",
        "targets": ["sqlite"]
        # "targets": ["sqlite", "windows_server_2016"]
    },
    "CVE-2017-10989": {
        "type" : "buffer_overflow",
        "targets": ["sqlite"]
    },
    "CVE-2018-10915": {
        "type" : "sql_injection",
        "targets": ["postgresql"]
        # "targets": ["postgresql", "enterprise_linux"]
    },
    "CVE-2019-10210": {
        "type" : "sql_injection",
        "targets": ["postgresql"]
        # "targets": ["postgresql", "windows_server_2016"]
    },
    "CVE-2019-2832": {
        "type" : "os_injection",
        "targets": ["solaris"]
    },
    "CVE-2016-3092": {
        "type" : "ddos",
        "targets": ["tomcat"]
    },
    "CVE-2018-7183": {
        "type" : "trojan",
        "targets": ["freebsd"]
    },
    "CVE-2019-5600": {
        "type" : "buffer_overflow",
        "targets": ["freebsd"]
    },
    "CVE-1999-0229": {
        "type" : "ddos",
        "targets": ["internet_information_server"]
    },
    "CVE-2000-0115": {
        "type" : "ddos",
        "targets": ["internet_information_server"]
    },
    "CVE-2019-2879": {
        "type" : "ddos",
        "targets": ["mysql"]
    },
    "CVE-2019-0221": {
        "type" : "xss",
        "targets": ["tomcat"]
    },
    "CVE-2011-1570": {
        "type" : "xss",
        "targets": ["tomcat"]
    },
    "CVE-2013-0942": {
        "type" : "xss",
        "targets": ["internet_information_server"]
    },
    "CVE-2014-3556": {
        "type" : "trojan",
        "targets": ["nginx"]
    },
    "CVE-2016-6664": {
        "type" : "worm",
        "targets": ["mariadb"]
    },
    "CVE-2017-7529": {
        "type" : "buffer_overflow",
        "targets": ["nginx"]
    },
    "CVE-2014-0133": {
        "type" : "buffer_overflow",
        "targets": ["nginx"]
    },
    "CVE-2010-3972": {
        "type" : "buffer_overflow",
        "targets": ["internet_information_server"]
    },
    "CVE-2010-2730": {
        "type" : "buffer_overflow",
        "targets": ["internet_information_server"]
    },
    "CVE-2010-1899": {
        "type" : "buffer_overflow",
        "targets": ["internet_information_server"]
    },
    "CVE-2016-1247": {
        "type" : "rootkit",
        "targets": ["nginx", "ubuntu_linux"]
    }
}

cwe_dict = {
    "ddos": ["tomcat", "nginx", "internet_information_server", "ubuntu_linux", "windows_server_2016", "solaris"],
    "csrf": ["tomcat", "nginx", "internet_information_server"],
    "redirect": ["tomcat", "nginx", "internet_information_server", "windows_server_2016"],
    "xss": ["tomcat", "nginx", "internet_information_server"],
    "sql_injection": ["postgresql", "sqlite", "mariadb"],
    "os_injection": ["ubuntu_linux", "windows_server_2016", "solaris", "enterprise_linux"],
    "trojan": ["ubuntu_linux", "windows_server_2016", "solaris", "enterprise_linux", "tomcat"],
    "worm": ["tomcat", "nginx", "internet_information_server"],
    "rootkit": ["tomcat", "nginx", "internet_information_server", "ubuntu_linux", "windows_server_2016", "solaris", "enterprise_linux"],
    "buffer_overflow": ["ubuntu_linux", "windows_server_2016", "solaris", "enterprise_linux", "nginx", "internet_information_server"]
    # "jamming": [],
    # "side_channel": []
}


def set_attacker_skills(level):
    global ATTACKER_SKILLS
    ATTACKER_SKILLS = level


# ogni attaccante è composto da un identificativo, una tipologia di attacco, un delay
# time ed una durata e l'attacco viene lanciato verso un indirizzo IPv4 random, 
# senza possibilità di detection da parte del framework. 
# class Attacker(Thread):

#     def __init__(self, idx, at, ts, dur, arb, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.id = idx
#         self.attack_type = at
#         self.time_sleep = ts
#         self.arbiter = arb
#         self.duration = dur
#         # self.duration = attack_durations.get(at)

    
#     def run(self):
#         # print("SLEEPING...")
#         time.sleep(self.time_sleep)
#         #print(bold(f'Attack scheduled'))
#         with open('active_conf.json') as f:
#             data = json.load(f)
#         n = data['num_dcomponents']
#         ips = []
#         targets = []

#         for i in range(n): # RECONNAISSANCE
#             if data['dcomponents'][0]['nodes'][i]['type'] == "VM" :
#                 ips.append(data['dcomponents'][0]['nodes'][i]['ipv4'])
        
#         target_ip = random.choice(ips)
#         # if self.attack_type == "ddos" :
#         targets = [random.choice(dbms_types + ws_types), random.choice(os_types)]
#         time.sleep(random.randrange(5, 11)) # WEAPONIZATION

#         print(red(f'Ongoing attack n.{self.id} of type {self.attack_type} with target {targets} hosted on {target_ip}'))
#         self.arbiter.predict(self.id, self.attack_type, target_ip, targets, data)
#         # self.arbiter.insertNotDet()
        
#         time.sleep(5 + self.duration) # EXPLOITATION
#         #arbiter = Arbiter()
#         self.arbiter.check(self.id, self.attack_type, target_ip, targets)


class Attacker(Thread):

    def __init__(self, idx, ts, mode, arb, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = idx
        # self.attack_type = None
        # self.taget_ip = tip
        self.time_sleep = ts
        self.arbiter = arb
        # self.duration = dur
        # self.duration = attack_durations.get(at)
        self.mode = mode
        with open('vulnConfs_static.json') as f:
            self.data_cve = json.load(f)

    
    def run(self):
        # print("SLEEPING...")
        time.sleep(self.time_sleep)
        #print(bold(f'Attack scheduled'))
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arbiter.ID_TEST)}.json') as f:
            data = json.load(f)
        n = data['num_dcomponents']
        ips = []
        # targets = []

        for i in range(n):
            if data['dcomponents'][0]['nodes'][i]['type'] == "VM" :
                ips.append(data['dcomponents'][0]['nodes'][i]['ipv4'])
        
        target_ip = random.choice(ips)
        # RECONNAISSANCE
        time.sleep(np.random.normal(15, ATTACKERS_EXPERTISE.get(ATTACKER_SKILLS), 1))
        # if self.mode == 1 :
        #     time.sleep(random.randrange(10, 13)) 
        # else :
        #     time.sleep(random.randrange(12, 20))
        # targets = [ random.choice(dbms_types + ws_types), random.choice(os_types) ]
        print(red(f'Ongoing attack n.{self.id} on {target_ip}'))
        av_targets = self.arbiter.checkRec(self.id, target_ip)
        # print(av_targets)
        
        if len(av_targets) > 0 :
            # print(av_targets)
            # target_cve = random.choice([ k for k in cve_targets.keys() if all(elem in av_targets for elem in cve_targets[k]['targets']) ])
            target_prod = random.choice([ av_targets[i][0] for i in range(len(av_targets)) ])
            target_id = [ item[1] for item in av_targets if (item[0] == target_prod) ][0]
            target_cve = random.choice([ k for k in self.data_cve.keys() if (target_prod in self.data_cve[k]['targets']) ])
            # self.attack_type = random.choice([ k for k in cwe_dict.keys() if (target_node in cwe_dict[k]) ])
            self.attack_type = self.data_cve[target_cve]['type']
            self.duration = np.random.normal(attack_durations.get(self.attack_type), 
                                ATTACKERS_EXPERTISE.get(ATTACKER_SKILLS), 1)

            print(red(f'Ongoing attack n.{self.id} of type {self.attack_type} based on {target_cve} led to {target_prod} on component n.{target_id}'))
            # self.arbiter.predict(self.id, self.attack_type, target_ip, targets, data)
            # self.arbiter.insertNotDet()
            if self.mode == 1 :
                self.arbiter.alert(self.id, self.attack_type, target_ip, target_cve)
            
            time.sleep(3 + self.duration) # WEAPONIZATION + DELIVERY + EXPLOITATION
            #arbiter = Arbiter()
            self.arbiter.checkExp(self.id, self.attack_type, target_ip, target_id, target_prod)
            # self.arbiter.check(self.id, self.attack_type, target_ip, targets)
        else :
            print(red(f'Attack n.{self.id} thwarted in RECONNAISSANCE phase'))


# ogni attaccante è composto da un identificativo, un identificativo CVE di riferimento 
# ed un delay time. Quest'ultimo è associato ad una tipologia di attacco, uno specifico
# target (vendor/product/version) ed una durata. Si ipotizza che questi attacchi vengano
# individuati da agenti esterni e comunicati al framework attraverso l'Alert Manager.
class DetectedAttacker(Thread):

    def __init__(self, idx, arb, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = idx
        # self.attack_type = at
        self.arbiter = arb
        # self.duration = dur
        # cve_ids = list(cve_targets.keys())
        # self.target_cve = random.choice(cve_ids)
        self.attack_type = None
        # self.attack_type = cve_targets.get(self.target_cve).get('type')

    
    def run(self):
        # time.sleep(self.time_sleep)
        # print(bold(f'Attack scheduled'))
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arbiter.ID_TEST)}.json') as f:
            data = json.load(f)
        n = data['num_dcomponents']
        ips = []

        for i in range(n): # RECONNAISSANCE
            if data['dcomponents'][0]['nodes'][i]['type'] == "VM" :
                ips.append(data['dcomponents'][0]['nodes'][i]['ipv4'])
        
        target_ip = random.choice(ips)

        # time.sleep(5)

        prods = self.arbiter.verifyScan(target_ip)
        if len(prods) > 0 :
            cve_ids = [ item for item in cve_targets.keys() if any(elem in cve_targets[item].get('targets') for elem in prods) ]
            if len(cve_ids) > 0 :
                target_cve = random.choice(cve_ids)
                self.attack_type = cve_targets.get(target_cve).get('type')
                arbiter.scheduled_attacks[attack_types.index(self.attack_type)] += 1
                
                self.duration = np.random.normal(attack_durations.get(self.attack_type), 
                                ATTACKERS_EXPERTISE.get(ATTACKER_SKILLS), 1)

                target_comps = cve_targets.get(target_cve).get('targets')
                time.sleep(random.randrange(2, 7)) # WEAPONIZATION
                self.arbiter.predict(self.id, self.attack_type, target_ip, target_comps, data)
                
                # time.sleep(5)

                print(red(f'Detected attack n.{self.id} of type {self.attack_type} based on {target_cve} led to {target_ip}'))
                # self.arbiter.insertDet()
                self.arbiter.alert(self.id, self.attack_type, target_ip, target_cve)
                time.sleep(self.duration)
                self.arbiter.check(self.id, self.attack_type, target_ip, target_comps)
        else :
            print(cyan(f'No vulnerabilities to exploit in {target_ip}'))
        
        time.sleep(12)


if __name__ == "__main__" :
    sl = int(input("Enter security level (1: low, 2: medium, 3: high): "))
    arb = arbiter.Arbiter(sl)
    arb.print_stats()