import json
import os
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
#  Python wrapper of CIRCL cve-search REST APIs (github)
from pycvesearch import CVESearch
import conf_manager as cm
from threading import Thread
from active_conf import activeConf

bold = lambda text: '\033[0;1m' + text + '\033[0m'
yellow = lambda text: '\033[0;33m' + text + '\033[0m'
listP_a = ["amazon_aws", "cloud_platform", "sqlite", "tomcat", "mariadb", "postgresql", "nginx", "internet_information_server"] 
# listP_a = ["amazon_aws", "cloud_platform", "mysql", "tomcat", "mariadb", "postgresql", "nginx", "internet_information_server"] 
listP_o = ["azure_devops_server", "windows_server_2016", "ubuntu_linux", "enterprise_linux", "freebsd", "solaris"]

# read JSON files of application deployment configurations
def read_conf(idx):
    with open('./docs/conf_{}.json'.format(idx)) as f:
        data = json.load(f)
    return data


class SecurityAssessmentUnit(object):

    def __init__(self):
        self.basepath = './docs/'
        self.secThreshold = 6.9
        self.max_score = 0
        self.activeConf = activeConf
        self.alive = True

    # SETUP STAGE - STATIC SECURITY ASSESSMENT
    # find the initial application deployment configuration to deploy
    # evaluate  average security risk score as average CVSS score of
    # CVE vulnerabilities found in CPE dictionary filtered by CPE 2.3 id
    # on total number of components deployed into configuration.
    def ss_secAssessment(self, datac):
        print("************** SETUP STAGE - STEP 1 - SECURITY ASSESSMENT **************")
        
        cve = CVESearch()
        asr = []
        lw_asr = []

        conf_gen = None
        with open('./conf_gen.json') as f:
            conf_gen = json.load(f)
        # conf_gen = read_conf('./conf_gen.json')
        
        n_dbms = conf_gen['num_dbms']
        # print(f'n_DBMS: {n_dbms}')
        n_csp = conf_gen['num_csp']
        # print(f'n_CSP: {n_csp}')
        n_ws = conf_gen['num_ws']
        # print(f'n_WS: {n_ws}')
        n_vm = conf_gen['num_vm']
        # print(f'n_VM: {n_vm}')

        dbms_vendors = [ conf_gen['dbms'][j]['vendor'] for j in range(n_dbms) ]
        dbms_products = [ conf_gen['dbms'][j]['product'] for j in range(n_dbms) ]
        dbms_versions = [ conf_gen['dbms'][j]['version'] for j in range(n_dbms) ]
        csp_vendors = [ conf_gen['csp'][j]['vendor'] for j in range(n_csp) ]
        csp_products = [ conf_gen['csp'][j]['product'] for j in range(n_csp) ]
        csp_versions = [ conf_gen['csp'][j]['version'] for j in range(n_csp) ]
        ws_vendors = [ conf_gen['ws'][j]['vendor'] for j in range(n_ws) ]
        ws_products = [ conf_gen['ws'][j]['product'] for j in range(n_ws) ]
        ws_versions = [ conf_gen['ws'][j]['version'] for j in range(n_ws) ]
        vm_vendors = []
        vm_products = []
        vm_versions = []
        for j in range(n_vm):
            if conf_gen['vm'][j]['vendor'] not in vm_vendors:
                vm_vendors.append(conf_gen['vm'][j]['vendor'])
            if conf_gen['vm'][j]['product'] not in vm_products:
                vm_products.append(conf_gen['vm'][j]['product'])
            if conf_gen['vm'][j]['version'] not in vm_versions:
                vm_versions.append(conf_gen['vm'][j]['version'])

        scores_all = []
        threads_all = []

        for j in range(n_dbms):
            def ss_dbms():
                tmp_scores = 0
                # if dbms_products[j] in ["mysql", "postgresql", "mariadb"] :
                search = 'cpe:2.3:a:' + '{}'.format(dbms_vendors[j]) + ':{}'.format(dbms_products[j]) + ':{}'.format(dbms_versions[j]) + ':*'
                # else:
                #    search = 'cpe:2.3:o:' + '{}'.format(dbms_vendors[j]) + ':{}'.format(dbms_products[j]) + ':{}'.format(dbms_versions[j]) + ':*'
                # print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(f'{search} --> {tmp_scores}')
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                    print(bold(f'{search} --> failed to lookup'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_dbms)
            threads_all.append(t)
            t.start()

        for j in range(n_csp):
            def ss_csp():
                tmp_scores = 0
                # amazon a, google a, microsoft ws16 o, mysql a, canonical o, apache a
                if csp_products[j] in ["amazon_aws", "cloud_platform"] :
                    search = 'cpe:2.3:a:' + '{}'.format(csp_vendors[j]) + ':{}'.format(csp_products[j]) + ':{}'.format(csp_versions[j]) + ':*'
                elif csp_products[j] in ["azure_devops_server"]:
                    search = 'cpe:2.3:o:' + '{}'.format(csp_vendors[j]) + ':{}'.format(csp_products[j]) + ':{}'.format(csp_versions[j]) + ':*'
                # print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(f'{search} --> {tmp_scores}')
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                    print(bold(f'{search} --> failed to lookup'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_csp)
            threads_all.append(t)
            t.start()

        for j in range(n_ws):
            def ss_ws():
                tmp_scores = 0
                #if ws_products[j] in ["tomcat"] :
                search = 'cpe:2.3:a:' + '{}'.format(ws_vendors[j]) + ':{}'.format(ws_products[j]) + ':{}'.format(ws_versions[j]) + ':*'
                #else:
                #    search = 'cpe:2.3:o:' + '{}'.format(ws_vendors[j]) + ':{}'.format(ws_products[j]) + ':{}'.format(ws_versions[j]) + ':*'
                # print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(f'{search} --> {tmp_scores}')
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                    print(bold(f'{search} --> failed to lookup'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_ws)
            threads_all.append(t)
            t.start()

        for j in range(len(vm_vendors)):
            def ss_vm():
                tmp_scores = 0
                # amazon a, google a, microsoft ws16 o, mysql a, canonical o, apache a
                #if vm_products[j] in ["windows_server_2016", "ubuntu_linux"] :
                search = 'cpe:2.3:o:' + '{}'.format(vm_vendors[j]) + ':{}'.format(vm_products[j]) + ':{}'.format(vm_versions[j]) + ':*'
                #else:
                #    search = 'cpe:2.3:a:' + '{}'.format(vm_vendors[j]) + ':{}'.format(vm_products[j]) + ':{}'.format(vm_versions[j]) + ':*'
                # print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(f'{search} --> {tmp_scores}')
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                    print(bold(f'{search} --> failed to lookup'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_vm)
            threads_all.append(t)
            t.start()
        
        for t in threads_all:
            t.join()
        
        threads_all.clear()
        threads_conf = []

        # for i in range(len(os.listdir(self.basepath))):
        for i in range(len(datac)):
            # conf = read_conf(i+1)
            # print("****NEW CONFIGURATION LOADED****")
            # n = conf['num_dcomponents']
            # vendors = [ conf['dcomponents'][0]['nodes'][i]['vendor'] for i in range(n) ]
            # products = [ conf['dcomponents'][0]['nodes'][i]['product'] for i in range(n) ]
            # versions = [ conf['dcomponents'][0]['nodes'][i]['version'] for i in range(n) ]
            # scores = 0
            # for j in range(n):
            #     tmp_scores = 0
            #     # amazon a, google a, microsoft ws16 o, mysql a, canonical o, apache a
            #     if products[j] in listP_a :
            #         search = 'cpe:2.3:a:' + '{}'.format(vendors[j]) + ':{}'.format(products[j]) + ':{}'.format(versions[j]) + ':*'
            #     elif products[j] in listP_o:
            #         search = 'cpe:2.3:o:' + '{}'.format(vendors[j]) + ':{}'.format(products[j]) + ':{}'.format(versions[j]) + ':*'
            #     #print(search)
            #     for (h, k) in scores_all:
            #         if search == h:
            #             tmp_scores += k
            #     # tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
            #     # print("%.1f" % tmp_scores)
            #     # if len(res) > 0 :
            #     #     scores += tmp_scores / len(res)
            #     scores += tmp_scores
            # asr.append(scores / n)
            # print("ASR related to conf_{}".format(i+1) + " = %.1f" % asr[i])
            def ss_conf(idx):
                # conf = read_conf(idx+1)
                conf = datac[idx]
                # print("****NEW CONFIGURATION LOADED****")
                n = conf['num_dcomponents']
                vendors = [ conf['dcomponents'][0]['nodes'][i]['vendor'] for i in range(n) ]
                products = [ conf['dcomponents'][0]['nodes'][i]['product'] for i in range(n) ]
                versions = [ conf['dcomponents'][0]['nodes'][i]['version'] for i in range(n) ]
                scores = 0
                for j in range(n):
                    tmp_scores = 0
                    # amazon a, google a, microsoft ws16 o, mysql a, canonical o, apache a
                    if products[j] in listP_a :
                        search = 'cpe:2.3:a:' + '{}'.format(vendors[j]) + ':{}'.format(products[j]) + ':{}'.format(versions[j]) + ':*'
                    elif products[j] in listP_o:
                        search = 'cpe:2.3:o:' + '{}'.format(vendors[j]) + ':{}'.format(products[j]) + ':{}'.format(versions[j]) + ':*'
                    #print(search)
                    for (h, k) in scores_all:
                        if search == h:
                            tmp_scores += k
                    # tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    # print("%.1f" % tmp_scores)
                    # if len(res) > 0 :
                    #     scores += tmp_scores / len(res)
                    scores += tmp_scores
                asr.append((idx, scores / n))
                # print("ASR related to conf_{}".format(idx+1) + " = %.1f" % (scores / n))
            t = Thread(target=ss_conf, args=(i,))
            threads_conf.append(t)
            t.start()
        
        for t in threads_conf:
            t.join()
        
        threads_conf.clear()
    
        # catch configurations with ASR < threshold
        for i, s in asr:
            if s <= self.secThreshold :
                lw_asr.append(i)
        # for i in asr:
        #     if i <= self.secThreshold :
        #         lw_asr.append(i)
        
        #print(lw_asr)
        return (lw_asr)
        
        # catch the configuration with lowest ASR
        #lw_asr = asr.index(min(asr)) + 1
        #print("Configuration with lowest Average Security Risk is n.%i" % lw_asr)
        #return lw_asr
        
        # GRAPICHS
        #sns.set()
        #_ = plt.hist(asr, density = 1, bins = len(asr))
        #_ = plt.title('average fitness per iteration')
        #_ = plt.xlabel('iterations')
        #_ = plt.ylabel('fitness')
        #plt.show()

    # PROACTIVE STAGE - ACTIVE DEPLOYMENT CONFIGURATION SECURITY ASSESSMENT
    def ps_secAssessment(self):
        while self.alive :
            cve = CVESearch()
            asr = 0
            max_cvss = 0

            conf = self.activeConf
            print(bold("************** ACTIVE CONFIGURATION RETRIEVED **************"))
            n = conf.num_dcomponents
            vpv = []
            for i in range(n):
                if (conf.dcomponents[i].vendor, conf.dcomponents[i].product, conf.dcomponents[i].version) not in vpv :
                    vpv.append((conf.dcomponents[i].vendor, conf.dcomponents[i].product, conf.dcomponents[i].version))
            # vendors = [ conf.dcomponents[i].vendor for i in range(n) ]
            # products = [ conf.dcomponents[i].product for i in range(n) ]
            # versions = [ conf.dcomponents[i].version for i in range(n) ]
            scores = []
            max_cvss_res = []
            threads_res = []
            for (i, j, h) in vpv:
                # tmp_scores = 0
                # if j in listP_a:
                #     search = 'cpe:2.3:a:' + '{}'.format(i) + ':{}'.format(j) + ':{}'.format(h) + ':*'
                # elif j in listP_o:
                #     search = 'cpe:2.3:o:' + '{}'.format(i) + ':{}'.format(j) + ':{}'.format(h) + ':*'
                # print(bold(search))
                # res = cve.cvefor(search)
                # tmp_scores += np.sum(np.asarray([res[k].get('cvss') for k in range(len(res))], dtype='float'))
                # #print("%.1f" % tmp_scores)
                # if len(res) > 0:
                #     scores += tmp_scores / len(res)
                def ps_dcomps(iv, jp, hv):
                    tmp_scores = 0
                    if j in listP_a:
                        search = 'cpe:2.3:a:' + '{}'.format(iv) + ':{}'.format(jp) + ':{}'.format(hv) + ':*'
                    elif j in listP_o:
                        search = 'cpe:2.3:o:' + '{}'.format(iv) + ':{}'.format(jp) + ':{}'.format(hv) + ':*'
                    # print(bold(search))
                    try:
                        res = cve.cvefor(search)
                        tmp_scores += np.sum(np.asarray([res[k].get('cvss') for k in range(len(res))], dtype='float'))
                        max_cvss_res.append(max(np.asarray([res[k].get('cvss') for k in range(len(res))], dtype='float')))
                        # print(f'{search} --> {tmp_scores}')
                        if len(res) > 0:
                            scores.append(tmp_scores / len(res))
                    except ValueError:
                        print(bold(f'{search} --> failed to lookup'))
                t = Thread(target=ps_dcomps, args=(i, j, h))
                threads_res.append(t)
                t.start()
            
            for t in threads_res:
                t.join()
            threads_res.clear()
            
            # asr = scores / n
            # max_cvss = max(np.asarray([res[k].get('cvss') for k in range(len(res))], dtype='float'))
            asr = sum(scores) / n
            max_cvss = max(max_cvss_res)
            # print("ASR della conf_{}".format(i+1) + " = %.1f" % asr)
            # print("MAX CVSS della conf_{}".format(i+1) + " = %.1f" % max_cvss)

            last_max = self.max_score
            self.max_score = max_cvss
            #return (asr, last_max)
                
            print(bold(f'ASR related to active configuration: {asr}'))
            print(bold(f'MAX CVSS related to active configureation: {self.max_score}'))

            # TODO part
            # evaluate max cvss score and Average Security Risk vector of
            # active application deployment configuration
            if last_max - self.max_score > 0.1 :
                print(yellow("<<<<<<<<<<< WARNING >>>>>>>>>>>"))
                # TODO: lancia l'ottimizzazione per la scelta di una nuova configurazione
                #       (nuova fase di setup stage)

            # GRAPHICS
            # sns.set()
            # _ = plt.hist(asr, density = 1, bins = len(asr))
            # _ = plt.title('average fitness per iteration')
            # _ = plt.xlabel('iterations')
            # _ = plt.ylabel('fitness')
            # plt.show()
        
        print(yellow("----------------- SECURITY ASSESSMENT UNIT STOPPED -----------------"))


    def retrieveVulnConfs(self, t_ip, cve):
        vuln_dcomps = []

        with open('./vulnConfs_static.json') as f:
            data = json.load(f)
        
        # cve_search = CVESearch()
        # res = cve_search.id(cve)
        # n_vc = len(res['vulnerable_configuration'])
        # vuln_confs = [ res['vulnerable_configuration'][j]['id'] for j in range(n_vc) ]
        # vuln_confs = data[cve]
        # print(vuln_confs)
        
        conf = self.activeConf
        host_name = None
        n = conf.num_dcomponents

        for i in range(n):
            if (conf.dcomponents[i].type == "VM" and conf.dcomponents[i].ip_addr == t_ip):
                host_name = conf.dcomponents[i].name

        vpv = []
        for i in range(n):
            if (conf.dcomponents[i].type == "VM" and conf.dcomponents[i].ip_addr == t_ip):
                if (conf.dcomponents[i].vendor, conf.dcomponents[i].product, conf.dcomponents[i].version) not in vpv :
                    vpv.append((conf.dcomponents[i].id, conf.dcomponents[i].product))
                    # vpv.append((conf.dcomponents[i].vendor, conf.dcomponents[i].product, conf.dcomponents[i].version))
            elif ((conf.dcomponents[i].type == "DBMS" or conf.dcomponents[i].type == "WS") and \
                conf.dcomponents[i].host == host_name):
                if (conf.dcomponents[i].vendor, conf.dcomponents[i].product, conf.dcomponents[i].version) not in vpv :
                    vpv.append((conf.dcomponents[i].id, conf.dcomponents[i].product))
                    # vpv.append((conf.dcomponents[i].vendor, conf.dcomponents[i].product, conf.dcomponents[i].version))
        
        # print(vpv)
        # for (idx, i, j, h) in vpv:
        for (idx, j) in vpv:
            [ vuln_dcomps.append((idx, j)) for target in data[cve]['targets'] if (j in target) ]
            # [ vuln_dcomps.append((idx, i, j, h)) for target in data[cve]['targets'] if (j in target) ]

        # print(vuln_dcomps)
        return vuln_dcomps
    

    def stop(self):
        self.alive = False
