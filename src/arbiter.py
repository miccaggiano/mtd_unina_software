import time, sys, json, random, os
import matplotlib.pyplot as plt
import seaborn as sns
import pickle as pk
import pandas as pd
import numpy as np
import scipy.optimize as sciopt
from threading import Thread
from scipy.stats import binom

magenta = lambda text: '\033[0;35m' + text + '\033[0m'
cyan = lambda text: '\033[0;36m' + text + '\033[0m'
pink = lambda text: '\033[0;95m' + text + '\033[0m'

MODE = 0
NUM_ATTACKS = 0
PERC_DET = 0
NUM_DETECTED_ATTACKS = NUM_ATTACKS * PERC_DET
NUM_RANDOM_ATTACKS = NUM_ATTACKS - NUM_DETECTED_ATTACKS

ID_TEST = 0
attack_types = {"scan": 0, "ddos": 1, "csrf": 2, "redirect": 3, "xss": 4, "sql_injection": 5, "os_injection": 6,
                "trojan": 7, "worm": 8, "rootkit": 9, "buffer_overflow": 10, "jamming": 11, "side_channel": 12}
scheduled_attacks = [ 0 for _ in range(len(attack_types)) ]


def fitFunc1(x, a1, a2, b1, b2, c1, c2):
	return (a1 * np.exp(-((x - b1) / c1)**2) + a2 * np.exp(-((x - b2) / c2)**2))

def fitFunc(x, a, b, c):
    return (a * np.exp(-b * x) + c)

def set_number_attacks(n):
    global NUM_ATTACKS
    NUM_ATTACKS = n

def set_perc_det(n):
    global PERC_DET
    PERC_DET = n

def set_id_test(n):
    global ID_TEST
    ID_TEST = n

def set_number_detected_attacks():
    global NUM_ATTACKS
    na = NUM_ATTACKS
    global PERC_DET
    pd = PERC_DET
    nda = int(na * pd)
    global NUM_DETECTED_ATTACKS
    NUM_DETECTED_ATTACKS = nda

def set_number_random_attacks():
    global NUM_ATTACKS
    na = NUM_ATTACKS
    global NUM_DETECTED_ATTACKS
    nda = NUM_DETECTED_ATTACKS
    global NUM_RANDOM_ATTACKS
    NUM_RANDOM_ATTACKS = na - nda

def get_id_test():
    global ID_TEST
    idt = ID_TEST
    return idt


class Arbiter(object):


    def __init__(self, sec_level, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.num_attempts = [ 0 for i in range(len(attack_types)) ]
        self.queue_scanAtt = [ [] for i in range(len(attack_types)) ]
        self.queue_predictions = [ [] for i in range(len(attack_types)) ]
        self.seq_attacks = []
        self.probs = []
        self.am_unit = None
        self.basepath = "./results"
        self.sec_lev = sec_level
    

    def print_stats(self):
        os.mkdir(f'{self.basepath}/{str(ID_TEST)}/')
        os.mkdir(f'{self.basepath}/{str(ID_TEST)}/graphs/')
        # n_attacks = NUM_ATTACKS
        # par_rate_succ = [ [] for i in range(len(attack_types)) ]
        rate_succ = [ 0 for i in range(len(attack_types)) ]
        for k in range(len(attack_types)) :
            # for h in range(len(self.queue_scanAtt[k])) :
            #     par_sum = sum(self.queue_scanAtt[k][:h])
            #     par_rate_succ[k].append(par_sum / (h + 1))
            if self.num_attempts[k] > 0 :
                rate_succ[k] = sum(self.queue_scanAtt[k]) / self.num_attempts[k]
            else :
                rate_succ[k] = 0
        
        n_succ_scan = sum(self.queue_scanAtt[0])
        n_succ_att = sum([ sum(self.queue_scanAtt[j]) for j in range(1, len(attack_types)) ])
        n_attempts = self.num_attempts[0]
        n_attemptsExp = sum(self.num_attempts[1:])

        print(cyan("____________ STATS ____________"))
        print(cyan(f'Total number of attacks: {n_attempts}'))
        print(cyan(f'Successfull reconnaissance: {n_succ_scan}/{n_attempts}'))
        print(cyan(f'Successfull exploits: {n_succ_att}/{n_attemptsExp}'))

        att_types = list(attack_types.keys())

        # par_rate_succ_stats = {
        #     "types": att_types,
        #     "par_rate_succ": par_rate_succ
        # }
        rate_succ_stats = {
            "types": att_types,
            "rate_succ": rate_succ
        }
        # att_rate_succ_stats = {"types": AT, "att_rate_succ": rate_succ}
        with open(f'{self.basepath}/{str(ID_TEST)}/tmp_pred.txt', 'wb') as f :
            pk.dump(self.queue_predictions, f, pk.HIGHEST_PROTOCOL)
        with open(f'{self.basepath}/{str(ID_TEST)}/tmp_att.txt', 'wb') as f :
            pk.dump(self.queue_scanAtt, f, pk.HIGHEST_PROTOCOL)

        # with open(f'{self.basepath}/{str(ID_TEST)}/tmp_par.txt', 'wb') as f :
        #     pk.dump(par_rate_succ_stats, f, pk.HIGHEST_PROTOCOL)
        with open(f'{self.basepath}/{str(ID_TEST)}/tmp_rate.txt', 'wb') as f :
            pk.dump(rate_succ_stats, f, pk.HIGHEST_PROTOCOL)
        with open(f'{self.basepath}/{str(ID_TEST)}/tmp_probs.txt', 'wb') as f :
            pk.dump(self.probs, f, pk.HIGHEST_PROTOCOL)

        # GRAPHICS
        # GRAPHICS
        # n_fig = len(os.listdir(self.basepath + "/" + str(ID_TEST) + "/graphs/"))

        if MODE == 0 :
            n_fig = 0
            for i in range(len(attack_types)):
                # n_fig = len(os.listdir(self.basepath + "/graphs/"))
                at = list(attack_types.keys())[i]
                n = 0
                if i == 0:
                    n = NUM_ATTACKS
                else:
                    n = NUM_ATTACKS / (len(attack_types) - 1)
                p = rate_succ[i]
                x = np.linspace(1, n, n)
                pdf = binom.pmf(x, n, p)
                cdf = binom.cdf(x, n, p)

                fig, axs = plt.subplots(2)
                fig.suptitle(f'MTD-enabled success rate\n\
                    Sec_lev = {self.sec_lev}, Total Attacks = {NUM_ATTACKS}, \
                    Detected Attacks = {NUM_DETECTED_ATTACKS}', fontsize=12)
                # plt.grid()
                axs[0].grid()
                axs[0].plot(x, pdf, 'b-')
                axs[0].set_title(f'pdf - {at}')
                axs[0].set_xlabel('')
                axs[0].set_ylabel('Success rate')
                axs[0].set_xlim(right=n)
                axs[1].grid()
                axs[1].plot(x, cdf, 'r-')
                axs[1].set_title(f'cdf - {at}')
                axs[1].set_xlabel('Number of attacks')
                axs[1].set_ylabel('Success rate')
                axs[1].set_xlim(right=n)

                fig.tight_layout(rect=[0, 0.03, 1, 0.9])
                fig.savefig(self.basepath + "/" + str(ID_TEST) + "/graphs/Figure_{}_eff.png".format(n_fig + 1))
                n_fig += 1
                # plt.show()

    
    def check(self, att_id, att_type, t_ip, t_list):
        self.num_attempts[attack_types.get(att_type)] += 1
        res_scan = False
        res_target = False
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(ID_TEST)}.json') as f:
            data = json.load(f)
        n = data['num_dcomponents']
        idx = 0
        for i in range(n):
            if data['dcomponents'][0]['nodes'][i]['type'] == "VM" and data['dcomponents'][0]['nodes'][i]['ipv4'] == t_ip :
                res_scan = True
                idx = i
                target_name = data['dcomponents'][0]['nodes'][i]['name']
        
        if res_scan :
            for i in range(n):
                if len(t_list) > 1 :
                    if data['dcomponents'][0]['nodes'][idx]['product'] == t_list[1] and \
                            data['dcomponents'][0]['nodes'][i]['type'] == "DBMS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True
                    if data['dcomponents'][0]['nodes'][idx]['product'] == t_list[1] and \
                            data['dcomponents'][0]['nodes'][i]['type'] == "WS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True
                else :
                    if data['dcomponents'][0]['nodes'][i]['type'] == "DBMS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True
                    if data['dcomponents'][0]['nodes'][i]['type'] == "WS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True

        print(magenta(f'### RESULTS FROM ATTACK N.{att_id} ON {t_ip} ###'))
        print(magenta(f'Scan result: {res_scan} - Attack result: {res_target}'))

        if res_scan is True :
            self.queue_scanAtt[attack_types.get("scan")].append(1)
        else :
            self.queue_scanAtt[attack_types.get("scan")].append(0)
        if res_target is True :
            self.queue_scanAtt[attack_types.get(att_type)].append(1)
        else :
            self.queue_scanAtt[attack_types.get(att_type)].append(0)
    

    def checkExp(self, att_id, att_type, t_ip, t_id, t_prod):
        res_target = False
        noTarget = True
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(ID_TEST)}.json') as f:
            data = json.load(f)
        n = data['num_dcomponents']
        t_name = None
        for i in range(n):
            if (data['dcomponents'][0]['nodes'][i]['type'] == "VM" and data['dcomponents'][0]['nodes'][i]['ipv4'] == t_ip):
                t_name = data['dcomponents'][0]['nodes'][i]['name']
                noTarget = False
        
        i = 0
        while (not res_target and i < n and not noTarget):
            if ((data['dcomponents'][0]['nodes'][i]['type'] == "DBMS" or \
                data['dcomponents'][0]['nodes'][i]['type'] == "WS") and \
                data['dcomponents'][0]['nodes'][i]['id_comp'] == t_id):
                if (data['dcomponents'][0]['nodes'][i]['product'] == t_prod and \
                    data['dcomponents'][0]['nodes'][i]['host'] == t_name):
                    res_target = True
            elif (data['dcomponents'][0]['nodes'][i]['type'] == "VM" and \
                data['dcomponents'][0]['nodes'][i]['id_comp'] == t_id and \
                data['dcomponents'][0]['nodes'][i]['ipv4'] == t_ip and \
                data['dcomponents'][0]['nodes'][i]['product'] == t_prod):
                res_target = True
            i += 1

        print(magenta(f'### RESULTS FROM ATTACK N.{att_id} ON {t_prod} HOSTED ON {t_ip}: {res_target} ###'))

        if noTarget is False :
            self.num_attempts[attack_types.get(att_type)] += 1
            self.queue_predictions[attack_types.get(att_type)].append(1)
            if res_target is True :
                self.queue_scanAtt[attack_types.get(att_type)].append(1)
            else :
                self.queue_scanAtt[attack_types.get(att_type)].append(0)
        else :
            self.num_attempts[attack_types.get("scan")] += 1
            self.queue_predictions[attack_types.get("scan")].append(1)
            self.queue_scanAtt[attack_types.get("scan")].append(0)
    

    def checkRec(self, att_id, t_ip) :
        self.num_attempts[attack_types.get("scan")] += 1
        self.queue_predictions[attack_types.get("scan")].append(1)
        res_scan = False
        av_targets = []
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(ID_TEST)}.json') as f:
            data = json.load(f)
        n = data['num_dcomponents']
        for i in range(n):
            target_name = None
            if data['dcomponents'][0]['nodes'][i]['type'] == "VM" and data['dcomponents'][0]['nodes'][i]['ipv4'] == t_ip :
                res_scan = True
                target_name = data['dcomponents'][0]['nodes'][i]['name']
                self.queue_scanAtt[attack_types.get("scan")].append(1)
                av_targets.append((data['dcomponents'][0]['nodes'][i]['product'], data['dcomponents'][0]['nodes'][i]['id_comp']))
        
        if res_scan :
            for i in range(n):
                if (data['dcomponents'][0]['nodes'][i]['type'] == "DBMS" or \
                    data['dcomponents'][0]['nodes'][i]['type'] == "WS") and \
                    data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                    av_targets.append((data['dcomponents'][0]['nodes'][i]['product'], data['dcomponents'][0]['nodes'][i]['id_comp']))
        else :
            self.queue_scanAtt[attack_types.get("scan")].append(0)
        
        print(magenta(f'### RECONNAISSANCE RESULTS FROM ATTACK N.{att_id} ON {t_ip}: {res_scan} ###'))
    
        return av_targets
        

    def predict(self, att_id, att_type, t_ip, t_list, data):
        res_scan = False
        res_target = False
        n = data['num_dcomponents']
        idx = 0
        for i in range(n):
            if data['dcomponents'][0]['nodes'][i]['type'] == "VM" and data['dcomponents'][0]['nodes'][i]['ipv4'] == t_ip :
                    res_scan = True
                    idx = i
                    target_name = data['dcomponents'][0]['nodes'][i]['name']
        
        if res_scan :
            # if data['dcomponents'][0]['nodes'][idx]['product'] == t_list[1] :
            for i in range(n):
                if len(t_list) > 1 :
                    if data['dcomponents'][0]['nodes'][idx]['product'] == t_list[1] and \
                            data['dcomponents'][0]['nodes'][i]['type'] == "DBMS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True
                    if data['dcomponents'][0]['nodes'][idx]['product'] == t_list[1] and \
                            data['dcomponents'][0]['nodes'][i]['type'] == "WS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True
                else :
                    if data['dcomponents'][0]['nodes'][i]['type'] == "DBMS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True
                    if data['dcomponents'][0]['nodes'][i]['type'] == "WS" and \
                            data['dcomponents'][0]['nodes'][i]['product'] == t_list[0] and \
                            data['dcomponents'][0]['nodes'][i]['host'] == target_name :
                        res_target = True

        print(pink(f'### PREDICTIONS FROM ATTACK N.{att_id} ON {t_ip} ###'))
        print(pink(f'Scan result: {res_scan} - Attack result: {res_target}'))

        if res_scan is True :
            self.queue_predictions[attack_types.get("scan")].append(1)
        else :
            self.queue_predictions[attack_types.get("scan")].append(0)
        if res_target is True :
            self.queue_predictions[attack_types.get(att_type)].append(1)
        else :
            self.queue_predictions[attack_types.get(att_type)].append(0)

    
    def alert(self, att_id, att_type, target_ip, target_cve):
        self.am_unit.alert(att_id, att_type, target_ip, target_cve)
    

    def checkEffectiveness(self):
        # with open(self.basepath + "/" + str(ID_TEST) + '/tmp_seqAtt.txt', 'wb') as f :
        #     pk.dump(self.seq_attacks, f, pk.HIGHEST_PROTOCOL)
        
        tot_attempts = NUM_ATTACKS
        n_p = len(self.queue_predictions)
        n_r = len(self.queue_scanAtt)
        n_succ_p = 0
        n_succ_att = 0

        eff_res = [ [] for _ in range(len(attack_types)) ]
        if n_p == n_r :
            for i in range(len(attack_types)):
                n_inP = len(self.queue_predictions[i])
                n_inR = len(self.queue_scanAtt[i])
                n_succ_p += sum(self.queue_predictions[i])
                n_succ_att += sum(self.queue_scanAtt[i])
                if n_inP == n_inR :
                    for j in range(n_inP):
                        if self.queue_predictions[i][j] - self.queue_scanAtt[i][j] == 1 :
                            eff_res[i].append(1)
                        else :
                            eff_res[i].append(0)
            
            par_rate_succ = [ [] for i in range(len(attack_types)) ]
            for k in range(len(attack_types)) :
                #n_attempts = sum(self.num_attempts[k])
                for h in range(len(eff_res[k])) :
                    par_sum = sum(eff_res[k][:h])
                    par_rate_succ[k].append(par_sum / (h + 1))

        att_types = list(attack_types.keys())

        par_rate_succ_stats = {
            "types": att_types,
            "par_rate_succ": par_rate_succ
        }
        
        with open(self.basepath + '/' + str(ID_TEST) + '/tmp_eff.txt', 'wb') as f :
            pk.dump(par_rate_succ_stats, f, pk.HIGHEST_PROTOCOL)
        
        data1 = pd.DataFrame(par_rate_succ_stats)

        print(pink('------------------------------------------------------------'))
        print(pink(f'TOTAL NUMBER OF ATTACKS: {tot_attempts}'))
        print(pink(f'NUMBER OF ATTACKS (REC + EXP) PREDICTED AS SUCCESSFULL: {n_succ_p}'))
        print(pink(f'NUMBER OF ATTACKS (REC + EXP) VERIFIED AS SUCCESSFULL: {n_succ_att}'))
        print(pink(f'NUMBER OF ATTACKS (SCAN) SUCCESSFULLY THWARTED: {sum(eff_res[0])}'))
        print(pink(f'NUMBER OF ATTACKS (NOT SCAN) SUCCESSFULLY THWARTED: {sum([ sum(eff_res[i]) for i in range(1, len(eff_res)) ])}'))

        # GRAPHICS
        n_fig = len(os.listdir(self.basepath + '/' + str(ID_TEST) + "/graphs/"))

        if MODE == 1 :
            sns.set(color_codes=True)
            sns.set_style('whitegrid')
            fig = plt.figure(figsize=(12, 12))
            fig.suptitle('MTD effectiveness')
            # Plot effectiveness in terms of thwarted scans
            plt.subplot(211)
            # data = data1['par_rate_succ'][0]
            data = par_rate_succ[0]
            x = np.linspace(1, tot_attempts, num=tot_attempts, dtype=int)
            plt.xlabel('Number of attacks')
            plt.ylabel('Success rate - reconnaissance')
            #f = np.poly1d(np.polyfit(x, data, 4))

            # Fitting with exponential
            popt, pcov = sciopt.curve_fit(fitFunc, x, data, p0=(1, 1e-3, 1))
            plt.plot(x, data, 'o', x, fitFunc(x, *popt), '-', x, 1, 'r-')
            # Fitting with gaussian
            # popt, pcov = sciopt.curve_fit(fitFunc1, x, data, p0=(1, 1, 1, 1, 1, 1), bounds=(0.1522, 377.3))
            # popt, pcov = sciopt.curve_fit(fitFunc1, x, data)
            # print(pink(f'POPT = {popt}'))
            # print(pink(f'PCOV = {pcov}'))
            # plt.plot(x, data, 'o', x, fitFunc1(x, *popt), '-')

            # plt.plot(x, data, 'o', x, f(x), '-')
            plt.ylim(top=1)
            plt.xlim(right=tot_attempts)

            # Plot effectiveness per attack type
            plt.subplot(212)
            sns.boxplot(data=data1['par_rate_succ'])
            x = np.linspace(0, len(data1['types']), len(data1['types']))
            plt.xticks(x, labels=data1['types'], rotation=45, horizontalalignment='right')
            plt.ylabel('Distribution of success rate')
            plt.xlabel('Attack types')
            plt.ylim(top=1)

            #print("N_FIG : {}".format(n_fig))
            fig.tight_layout()
            fig.savefig(self.basepath + "/graphs/Figure_{}.png".format(n_fig + 1))
            plt.show()

            # Print r-squared error and SSE
            modelPredictions = fitFunc(x, *popt) 
            absError = modelPredictions - data
            SE = np.square(absError) # squared errors
            MSE = np.mean(SE) # mean squared errors
            RMSE = np.sqrt(MSE) # Root Mean Squared Error, RMSE
            Rsquared = 1.0 - (np.var(absError) / np.var(data))
            print(pink(f'SSE : {np.sum(SE)}'))
            print(pink(f'RMSE: {RMSE}'))
            print(pink(f'R-squared: {Rsquared}'))
        
        elif MODE == 0 :
            n_fig = 0
            for i in range(len(attack_types)):
                # n_fig = len(os.listdir(self.basepath + "/graphs/"))
                at = list(attack_types.keys())[i]
                n = 0
                if i == 0:
                    n = NUM_ATTACKS
                else:
                    n = NUM_ATTACKS / (len(attack_types) - 1)
                p = par_rate_succ[i][-1]
                x = np.linspace(1, n, n)
                pdf = binom.pmf(x, n, p)
                cdf = binom.cdf(x, n, p)

                fig, axs = plt.subplots(2)
                fig.suptitle(f'MTD/not-MTD comparison success rate\n\
                    Sec_lev = {self.sec_lev}, Total Attacks = {NUM_ATTACKS}, Detected Attacks = {NUM_DETECTED_ATTACKS}', fontsize=12)
                # plt.grid()
                axs[0].grid()
                axs[0].plot(x, pdf, 'b-')
                axs[0].set_title(f'pdf - {at}')
                axs[0].set_xlabel('')
                axs[0].set_ylabel('Success rate')
                axs[0].set_xlim(right=n)
                axs[1].grid()
                axs[1].plot(x, cdf, 'r-')
                axs[1].set_title(f'cdf - {at}')
                axs[1].set_xlabel('Number of attacks')
                axs[1].set_ylabel('Success rate')
                axs[1].set_xlim(right=n)

                fig.tight_layout(rect=[0, 0.03, 1, 0.9])
                fig.savefig(self.basepath + "/" + str(ID_TEST) + "/graphs/Figure_{}_comp.png".format(n_fig + 1))
                n_fig += 1
                # plt.show()


    def verifyScan(self, old_ip):
        res_scan = False
        res_prod = []
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(ID_TEST)}.json') as f:
            data = json.load(f)
        n = data['num_dcomponents']
        name_dcomp = None
        for i in range(n):
            if data['dcomponents'][0]['nodes'][i]['type'] == "VM" and \
                data['dcomponents'][0]['nodes'][i]['ipv4'] == old_ip :
                res_scan = True
                name_dcomp = data['dcomponents'][0]['nodes'][i]['name']
                res_prod.append(data['dcomponents'][0]['nodes'][i]['product'])
        
        if res_scan :
            for i in range(n):
                if (data['dcomponents'][0]['nodes'][i]['type'] == "DBMS" or \
                    data['dcomponents'][0]['nodes'][i]['type'] == "WS") and \
                    data['dcomponents'][0]['nodes'][i]['host'] == name_dcomp :
                    res_prod.append(data['dcomponents'][0]['nodes'][i]['product'])
        
        return res_prod

    
    def print_effProbs(self, m, s, att, def_sl):
        print("Totally it tooks {} minutes and {} seconds.".format(m, s))
        os.mkdir(f'{self.basepath}/{str(ID_TEST)}/')
        os.mkdir(f'{self.basepath}/{str(ID_TEST)}/graphs/')

        # tot_attempts = NUM_ATTACKS
        n_p = len(self.queue_predictions)
        n_r = len(self.queue_scanAtt)

        eff_probs = [ 0.0 for _ in range(len(attack_types)) ]
        errors = [ 0 for _ in range(len(attack_types) + 1) ]
        if n_p == n_r :
            for i in range(len(attack_types)):
                n_inP = len(self.queue_predictions[i])
                n_inR = len(self.queue_scanAtt[i])
                if n_inP == n_inR :
                    eff_mtd = [ 1 for j in range(n_inP) if (self.queue_predictions[i][j] - self.queue_scanAtt[i][j] == 1) ]
                    if n_inR > 0 :
                        eff_probs[i] = sum(eff_mtd) / n_inR
                    else :
                        eff_probs[i] = 0.0
                else :
                    errors[i] += 1
        else :
            errors[-1] += 1
        
        att_types = list(attack_types.keys())

        eff_probs_stats = {
            "types": att_types,
            "eff_probs": eff_probs,
            "errors": errors,
            "attempts": self.num_attempts,
            "test": {
                "id": ID_TEST,
                "num_att": NUM_ATTACKS,
                "perc_det": PERC_DET,
                "exp_att": att,
                "exp_def": def_sl
            },
            "duration": {
                "minutes": m,
                "seconds": s
            }
        }

        with open(f'{self.basepath}/{str(ID_TEST)}/tmp_pred.txt', 'wb') as f :
            pk.dump(self.queue_predictions, f, pk.HIGHEST_PROTOCOL)
        with open(f'{self.basepath}/{str(ID_TEST)}/tmp_att.txt', 'wb') as f :
            pk.dump(self.queue_scanAtt, f, pk.HIGHEST_PROTOCOL)
        with open(f'{self.basepath}/{str(ID_TEST)}/tmp_probs.txt', 'wb') as f :
            pk.dump(self.probs, f, pk.HIGHEST_PROTOCOL)
        with open(self.basepath + '/' + str(ID_TEST) + '/tmp_effProbs.txt', 'wb') as f :
            pk.dump(eff_probs_stats, f, pk.HIGHEST_PROTOCOL)
        
        #GRAPHICS
        at = list(attack_types.keys())
        fig = plt.figure(figsize=(12, 12))
        x = np.linspace(0, len(at), len(at))
        bars = plt.bar(at, eff_probs)
        plt.title(f'MTD effectiveness rate per attack type\n\
                Sec_lev = {self.sec_lev}, Total Attacks = {NUM_ATTACKS}, \
                Detected Attacks = {NUM_DETECTED_ATTACKS}', fontsize=12)
        plt.grid()
        plt.xticks(x, labels=at, rotation=45, horizontalalignment='right')
        plt.ylabel('Success rate')
        plt.xlabel('Attack types')
        plt.ylim(top=1)
        xlocs, _ = plt.xticks()
        for bar in bars:
            yval = bar.get_height()
            plt.text(bar.get_x(), yval + 0.015, yval)
        fig.tight_layout()
        fig.savefig(self.basepath + "/" + str(ID_TEST) + "/graphs/Figure_eff_at.png")
        # plt.show()
        
