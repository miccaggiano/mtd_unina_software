from pycvesearch import CVESearch
import json

cwe_dict = {
    "ddos": ["CWE-400", "CWE-772"],
    "csrf": ["CWE-352"],
    "redirect": ["CWE-601"],
    "xss": ["CWE-79"],
    "sql_injection": ["CWE-89", "CWE-94"],
    "os_injection": ["CWE-78"],
    "trojan": ["CWE-20", "CWE-507"],
    "worm": ["CWE-509"],
    "rootkit": ["CWE-426"],
    "buffer_overflow": ["CWE-119", "CWE-125"],
    "jamming": [],
    "side_channel": []
}

os_dict =	{
    "windows_server_2016": {
        "vendor": "microsoft",
		"product": "windows_server_2016",
		"version": ["1709", "1803", "1903"]
	},
	"enterprise_linux": {
        "vendor": "redhat",
        "product": "enterprise_linux",
        "version": ["7.3", "7.5", "8.0", "6.0"]
    },
    "freebsd": {
        "vendor": "freebsd",
        "product": "freebsd",
        "version": ["10.3", "7.3", "9.2", "6.0"]
    },
    "solaris": {
        "vendor": "oracle",
        "product": "solaris",
        "version": ["9", "10"]
    }
}

dbms_dict =	{
    "sqlite": {
        "vendor": "sqlite",
        "product": "sqlite",
        "version": ["3.8.2", "3.25.02", "3.7.17"]
    },
    "postgresql": {
        "vendor": "postgresql",
        "product": "postgresql",
        "version": ["9.4.7", "10.0", "9.6.1"]
    },
    "mariadb": {
        "vendor": "mariadb",
        "product": "mariadb",
        "version": ["10.1.9", "10.1.13", "10.0.2"]
    }
}

ws_dict =	{
    "tomcat": {
        "vendor": "apache",
        "product": "tomcat",
        "version": ["7.0.27", "8.5.14", "7.0.70"]
    },
    "nginx": {
        "vendor": "nginx",
        "product": "nginx",
        "version": ["1.5.2", "1.11.13"]
    },
    "internet_information_server": {
        "vendor": "microsoft",
        "product": "internet_information_server",
        "version": ["7.5", "5.1", "6.0"]
    }
}

cve_search = CVESearch()
data = {}
listOS = os_dict.keys()
listDBMS = dbms_dict.keys()
listWS = ws_dict.keys()

for os in listOS:
    print(os)

    vendor = os_dict[os]['vendor']
    product = os_dict[os]['product']
    search = '{}/{}'.format(vendor, product)
    try:
        res = cve_search.search(search)
        for k in cwe_dict.keys():
            vuln_confs = [ res['results'][i]['id'] for i in range(len(res['results'])) if res['results'][i]['cwe'] in cwe_dict[k] ]
            data[product] = {
                "type": k,
                "cve": vuln_confs
            }
    except:
        print(f'{search} --> failed to lookup')

for dbms in listDBMS:
    print(dbms)

    vendor = dbms_dict[dbms]['vendor']
    product = dbms_dict[dbms]['product']
    search = '{}/{}'.format(vendor, product)
    try:
        res = cve_search.search(search)
        for k in cwe_dict.keys():
            vuln_confs = [ res['results'][i]['id'] for i in range(len(res['results'])) if res['results'][i]['cwe'] in cwe_dict[k] ]
            data[product] = {
                "type": k,
                "cve": vuln_confs
            }
    except:
        print(f'{search} --> failed to lookup')

for ws in listWS:
    print(ws)

    vendor = ws_dict[ws]['vendor']
    product = ws_dict[ws]['product']
    search = '{}/{}'.format(vendor, product)
    try:
        res = cve_search.search(search)
        for k in cwe_dict.keys():
            vuln_confs = [ res['results'][i]['id'] for i in range(len(res['results'])) if res['results'][i]['cwe'] in cwe_dict[k] ]
            data[product] = {
                "type": k,
                "cve": vuln_confs
            }
    except:
        print(f'{search} --> failed to lookup')

with open('vulnProds_static.json', 'w') as f:
    json.dump(data, f)