import json
import os, time, sys
from random import randint
from threading import Thread
import active_conf as ac

import conf_generator as cg
import cost_opt_unit as cou
import sec_assess_unit as sau
import rec_engine as de
import enforcement_unit as eu
import att_hist_collector as atc
import arbiter as arb

cyan = lambda text: '\033[0;36m' + text + '\033[0m'

attack_types = {"scan": 0, "ddos": 1, "csrf": 2, "redirect": 3, "xss": 4, "sql_injection": 5, "os_injection": 6,
                "trojan": 7, "worm": 8, "rootkit": 9, "buffer_overflow": 10, "jamming": 11, "side_channel": 12}

react = False

# CONFIGURATION MANAGER
# Tasks:
# - select setup application deployment configuration
# - collect alerts and start reactive stage
# - assess security and risk management onto active configuration
class ConfigurationManager(object):

    #__basepath = './docs/'
    def __init__(self, ss_budget):
        self.att_coll = atc.AttackHistoryCollector(13)
        #self.att_detected = []
        self.budget = ss_budget
        self.alive = True

    # print JSON files of active application deployment configurations


    def print_active(self):
        print("---- Online configuration ----")
        #idx = self.activeConf.id
        # if os.path.exists('active_conf.json') :
        #     with open('active_conf.json') as f:
        if os.path.exists(f'active_conf_{str(arb.ID_TEST)}.json') :
            with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
                data = json.load(f)

            print(json.dumps(data, indent=4, sort_keys=True))


    # SETUP STAGE
    # Generates configurations in an highly interactive way and selects
    # the most feasible to be deployed during warm-up.
    def run_setupStage(self, datac, nconfs) :
        print("********* MTD framework to secure cloud applications *********")
        print("*****************  SETUP STAGE  ******************************")

        # economical constraints
        #budget = int(input("Enter your budget: "))
        budget = self.budget
        costs = [ randint(10, 25) for i in range(nconfs) ]

        # select first configuration

        # security assessment of generated configurations to
        # evaluate average security risks
        sec_unit = sau.SecurityAssessmentUnit()
        lw_risk_scores = sec_unit.ss_secAssessment(datac)
        # lw_risk_scores = [5.2, 5.3, 4.2, 5.2, 4.4, 5.2]

        # cost optimization for setup stage among generated configurations
        copt_unit = cou.CostOptimizationUnit()
        active_id = copt_unit.ss_costOpt(lw_risk_scores, nconfs, costs, budget)

        # activeConf = ac.ActiveConfiguration(datac[active_id], active_id)
        ac.set_activeConf(datac[active_id], active_id)
        self.activeConf = ac.activeConf
        print("*****************************")
        print("*New configuration activated!*")
        print("*** CONF_{} is now online ***".format(active_id))
        sec_unit.activeConf = ac.activeConf

        return self.activeConf


    # PROACTIVE STAGE
    def run_proactiveStage(self, security_level, enf_unit, det_queue):
        print("********* MTD framework to secure cloud applications *********")
        print("***************  PROACTIVE STAGE  ****************************")

        iterations = 0
        while self.alive :
            iterations += 1
            # react = False
            # nd = self.activeConf.num_dcomponents
            # num_mutations = security_level * 2 * nd
            # while (iterations < num_mutations) :
            # print(f'ONGOING DETECTED ATTACKS: {det_queue.qsize()}')
            # if det_queue.empty() is True :
            global react
            if (det_queue.empty() and not react) :
                # run proactive MTD selection
                att_pa = self.att_coll.getProbs()
                # att_pa = [.09,.070,.077,.077,.077,.077,.077,.077,.077,.077,.077,.071,.076]
                mgm_engine = de.ReconfigurationEngine()
                # mgm_engine.activeConf = self.activeConf
                mgm_engine.activeConf = ac.activeConf
                best_ind = mgm_engine.evaluateMTD(security_level, att_pa)
                print("Best overall strategy n.%i is %s" % (iterations, best_ind))

                if security_level == 1 :
                    if eu.counter_low > 3:
                        if (best_ind[0] or best_ind[2] or best_ind[3] or best_ind[4]) == 1:
                            best_ind[1] = 1
                    enf_unit.increment_counter_low()                  

                if best_ind[10] == 1:
                    if eu.counter < 5:
                        best_ind[10] = 0
                        best_ind[1] = 1
                    enf_unit.increment_counter()

                mgm_engine.enforceMTD(best_ind, enf_unit)
            else :
                react = True
                # Update attack probabilities and start reactive stage
                # (att_id, att_type, target_ip, cve) = det_queue.get_nowait()
                # self.run_reactiveStage(security_level, enf_unit, att_id, att_type, target_ip, cve)
                react = self.run_reactiveStage(security_level, enf_unit, det_queue, react)
                # print(react)
                # if (react is None or react is True):
                #     print(cyan("ERROR IN REACTIVE STAGE"))
                #     sys.exit(0)
            if security_level == 1:
                time.sleep(12)
            elif security_level == 2:
                time.sleep(8)
            elif security_level == 3:
                time.sleep(6)
        # self.print_active()


    def run_reactiveStage(self, security_level, enf_unit, det_queue, react):
        print("***************  REACTIVE STAGE  ****************************")
        # react = True
        # conf = self.activeConf
        conf = ac.activeConf
        # Retrieve attack from list
        (att_id, att_type, target_ip, cve) = det_queue.get_nowait()
        # (att_id, att_type, target_ip, cve) = det_queue.get()
        # self.att_coll.update(attack_types.get(att_type))
        # att_pa = self.att_coll.getProbs()
        #print("DETECTED: {}".format((att_id, att_type, target_ip, cve)))
        #del self.att_detected[0]

        # Retrieve information about CPE signatures of vulnerable configurations against CVE
        sec_unit = sau.SecurityAssessmentUnit()
        sec_unit.activeConf = conf
        print(cyan("---------- Searching for active component vulnerable to {} ----------".format(cve)))
        vuln_dcomps = sec_unit.retrieveVulnConfs(target_ip, cve)
        # print(vuln_dcomps)
        if len(vuln_dcomps) == 0 :
            print(cyan("---------- No vulnerable active component ----------"))
            react = False
        else :
            n = conf.num_dcomponents
            # self.print_active()
            # host_name = None

            for i in range(n):
                # print(conf.dcomponents[i].ip_addr)
                if (conf.dcomponents[i].type == "VM" and conf.dcomponents[i].ip_addr == target_ip) :
                    host_name = conf.dcomponents[i].name
                    self.att_coll.update(attack_types.get("scan"))

            # for (idx, j, k, h) in vuln_dcomps:
            for (idx, j) in vuln_dcomps:
                i = 0
                while (i < n and react):
                    # if ((conf.dcomponents[i].type == "DBMS" or conf.dcomponents[i].type == "WS") and \
                    #     (conf.dcomponents[i].vendor == j and conf.dcomponents[i].product == k and \
                    #     conf.dcomponents[i].version == h and conf.dcomponents[i].host == host_name)) or \
                    #     (conf.dcomponents[i].type == "VM" and conf.dcomponents[i].vendor == j and \
                    #     conf.dcomponents[i].product == k and conf.dcomponents[i].version == h):
                    if (conf.dcomponents[i].id == idx and conf.dcomponents[i].product == j):
                        self.att_coll.update(attack_types.get(att_type))
                        # att_pa = self.att_coll.getProbs()
                        # att_aw = [ 0 for _ in range(len(attack_types)) ]
                        # att_aw[attack_types.get("scan")] = 1
                        # att_aw[attack_types.get(att_type)] = 1
                        mgm_engine = de.ReconfigurationEngine()
                        mgm_engine.activeConf = conf
                        # mgm_engine.activeConf = self.activeConf
                        # best_ind = mgm_engine.evaluateMTD(security_level, att_pa, att_aw)
                        # best_ind = mgm_engine.evaluateMTD(security_level, att_pa)
                        strategies = [ 0 for i in range(len(attack_types)) ]
                        if att_type in [ "ddos", "csrf", "redirect", "trojan", "worm", "rootkit", "buffer_overflow" ]:
                            strategies[9] = 1
                        # if att_type in [ "ddos", "redirect", "worm" ]:
                        #     strategies[1] = 1
                        if att_type in [ "xss" ]:
                            strategies[6] = 1
                        if att_type in [ "sql_injection" ]:
                            strategies[8] = 1
                        if att_type in [ "os_injection" ]:
                            strategies[7] = 1
                        print(cyan("Best reacting strategy against attack n.%i is %s" % (att_id, strategies)))
                        # mgm_engine.enforceMTD(strategies, enf_unit, target_id=conf.dcomponents[i].id)
                        mgm_engine.enforceMTD(strategies, enf_unit, target_id=idx)
                        
                        det_queue.task_done()
                        react = False
                    i += 1
        
        return react


    def stop(self):
        self.alive = False

