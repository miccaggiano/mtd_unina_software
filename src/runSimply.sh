#!/bin/bash

start_time=$(date "+%Y.%m.%d-%H.%M.%S")
echo "Start time : $start_time"
counter=69
SECONDS=0

while [ $counter -le 82 ]
do
if [ $counter == 4 ]; then
    python3 run.py $counter 144 0.25 LOW 2
elif [ $counter == 5 ]; then
    python3 run.py $counter 288 0.25 LOW 2
elif [ $counter == 6 ]; then
    python3 run.py $counter 432 0.25 LOW 2
elif [ $counter == 7 ]; then
    python3 run.py $counter 576 0.25 LOW 2
elif [ $counter == 8 ]; then
    python3 run.py $counter 144 0.25 HIGH 2
elif [ $counter == 9 ]; then
    python3 run.py $counter 288 0.25 HIGH 2
elif [ $counter == 10 ]; then
    python3 run.py $counter 432 0.25 HIGH 2
elif [ $counter == 11 ]; then
    python3 run.py $counter 576 0.25 HIGH 2
elif [ $counter == 12 ]; then
    python3 run.py $counter 144 0.25 LOW 1
elif [ $counter == 13 ]; then
    python3 run.py $counter 288 0.25 LOW 1
elif [ $counter == 14 ]; then
    python3 run.py $counter 432 0.25 LOW 1
elif [ $counter == 15 ]; then
    python3 run.py $counter 576 0.25 LOW 1
elif [ $counter == 16 ]; then
    python3 run.py $counter 144 0.5 LOW 2
elif [ $counter == 17 ]; then
    python3 run.py $counter 288 0.5 LOW 2
elif [ $counter == 18 ]; then
    python3 run.py $counter 432 0.5 LOW 2
elif [ $counter == 19 ]; then
    python3 run.py $counter 576 0.5 LOW 2
elif [ $counter == 20 ]; then
    python3 run.py $counter 144 0.25 HIGH 1
elif [ $counter == 21 ]; then
    python3 run.py $counter 288 0.25 HIGH 1
elif [ $counter == 22 ]; then
    python3 run.py $counter 432 0.25 HIGH 1
elif [ $counter == 23 ]; then
    python3 run.py $counter 576 0.25 HIGH 1
elif [ $counter == 24 ]; then
    python3 run.py $counter 144 0.25 LOW 1
elif [ $counter == 25 ]; then
    python3 run.py $counter 288 0.25 LOW 1
elif [ $counter == 26 ]; then
    python3 run.py $counter 432 0.25 LOW 1
elif [ $counter == 27 ]; then
    python3 run.py $counter 576 0.25 LOW 1
elif [ $counter == 28 ]; then
    python3 run.py $counter 144 0.0 LOW 1
elif [ $counter == 29 ]; then
    python3 run.py $counter 288 0.0 LOW 1
elif [ $counter == 30 ]; then
    python3 run.py $counter 432 0.0 LOW 1
elif [ $counter == 31 ]; then
    python3 run.py $counter 576 0.0 LOW 1
elif [ $counter == 32 ]; then
    python3 run.py $counter 144 0.0 LOW 2
elif [ $counter == 33 ]; then
    python3 run.py $counter 288 0.0 LOW 2
elif [ $counter == 34 ]; then
    python3 run.py $counter 432 0.0 LOW 2
elif [ $counter == 35 ]; then
    python3 run.py $counter 576 0.0 LOW 2
elif [ $counter == 36 ]; then
    python3 run.py $counter 144 0.0 HIGH 1
elif [ $counter == 37 ]; then
    python3 run.py $counter 288 0.0 HIGH 1
elif [ $counter == 38 ]; then
    python3 run.py $counter 432 0.0 HIGH 1
elif [ $counter == 39 ]; then
    python3 run.py $counter 576 0.0 HIGH 1
elif [ $counter == 40 ]; then
    python3 run.py $counter 144 0.0 HIGH 2
elif [ $counter == 41 ]; then
    python3 run.py $counter 288 0.0 HIGH 2
elif [ $counter == 42 ]; then
    python3 run.py $counter 432 0.0 HIGH 2
elif [ $counter == 43 ]; then
    python3 run.py $counter 576 0.0 HIGH 2
elif [ $counter == 44 ]; then
    python3 run.py $counter 144 0.0 LOW 1
elif [ $counter == 45 ]; then
    python3 run.py $counter 432 0.0 HIGH 2
elif [ $counter == 46 ]; then
    python3 run.py $counter 432 0.25 LOW 1
elif [ $counter == 47 ]; then
    python3 run.py $counter 432 0.25 HIGH 1
elif [ $counter == 48 ]; then
    python3 run.py $counter 288 0.25 LOW 1
elif [ $counter == 49 ]; then
    python3 run.py $counter 288 0.25 HIGH 1
elif [ $counter == 50 ]; then
    python3 run.py $counter 576 0.25 HIGH 1
elif [ $counter == 51 ]; then
    python3 run.py $counter 144 0.5 LOW 1
elif [ $counter == 52 ]; then
    python3 run.py $counter 144 0.5 LOW 2
elif [ $counter == 53 ]; then
    python3 run.py $counter 144 0.5 HIGH 1
elif [ $counter == 54 ]; then
    python3 run.py $counter 144 0.5 HIGH 2
elif [ $counter == 55 ]; then
    python3 run.py $counter 288 0.5 LOW 1
elif [ $counter == 56 ]; then
    python3 run.py $counter 288 0.5 LOW 2
elif [ $counter == 57 ]; then
    python3 run.py $counter 288 0.5 HIGH 1
elif [ $counter == 58 ]; then
    python3 run.py $counter 288 0.5 HIGH 2
elif [ $counter == 59 ]; then
    python3 run.py $counter 432 0.5 LOW 1
elif [ $counter == 60 ]; then
    python3 run.py $counter 432 0.5 LOW 2
elif [ $counter == 61 ]; then
    python3 run.py $counter 432 0.5 HIGH 1
elif [ $counter == 62 ]; then
    python3 run.py $counter 432 0.5 HIGH 2
elif [ $counter == 63 ]; then
    python3 run.py $counter 576 0.5 LOW 1
elif [ $counter == 64 ]; then
    python3 run.py $counter 576 0.5 LOW 2
elif [ $counter == 65 ]; then
    python3 run.py $counter 576 0.5 HIGH 1
elif [ $counter == 66 ]; then
    python3 run.py $counter 576 0.5 HIGH 2
elif [ $counter == 67 ]; then
    python3 run.py $counter 144 0.25 LOW 1
elif [ $counter == 68 ]; then
    python3 run.py $counter 144 0.25 HIGH 1
elif [ $counter == 69 ]; then
    python3 run.py $counter 72 0.0 LOW 1
elif [ $counter == 70 ]; then
    python3 run.py $counter 72 0.0 LOW 2
elif [ $counter == 71 ]; then
    python3 run.py $counter 72 0.0 HIGH 1
elif [ $counter == 72 ]; then
    python3 run.py $counter 72 0.0 HIGH 2
elif [ $counter == 73 ]; then
    python3 run.py $counter 72 0.25 LOW 1
elif [ $counter == 74 ]; then
    python3 run.py $counter 72 0.25 LOW 2
elif [ $counter == 75 ]; then
    python3 run.py $counter 72 0.25 HIGH 1
elif [ $counter == 76 ]; then
    python3 run.py $counter 72 0.25 HIGH 2
elif [ $counter == 77 ]; then
    python3 run.py $counter 72 0.5 LOW 1
elif [ $counter == 78 ]; then
    python3 run.py $counter 72 0.5 LOW 2
elif [ $counter == 79 ]; then
    python3 run.py $counter 72 0.5 HIGH 1
elif [ $counter == 80 ]; then
    python3 run.py $counter 72 0.5 HIGH 2
elif [ $counter == 81 ]; then
    python3 run.py $counter 432 0.25 LOW 2
elif [ $counter == 82 ]; then
    python3 run.py $counter 432 0.25 HIGH 2
fi

((counter++))
done

duration=$SECONDS
end_time=$(date "+%Y.%m.%d-%H.%M.%S")
echo
echo
echo "Start Time : $start_time"
echo
echo "End Time : $end_time"
echo
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
echo "All done"
