import itertools as it
from threading import Thread
import time, sys

class Attacker(Thread):

    def __init__(self, idx, ts,  *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = idx
        self.time_sleep = ts
    
    def run(self):
        print("SLEEPING...")
        time.sleep(self.time_sleep)
        print(f'END OF THREAD-{self.id}')


threads_att = []
threads_det = []
nra = 144
nda = 36

for i, j in it.zip_longest(range(nra), range(nda)):
    if i is not None :
        if i > 0 :
            if i < nda :
                attacker = Attacker(i + 1, 5 * i)
            else :
                attacker = Attacker(i + 1, 3 * i)
        else :
            attacker = Attacker(i + 1, 2)
        print(f'ATTACKER N.{i + 1} SLEEP: {attacker.time_sleep}')
        threads_att.append(attacker)
    if j is not None :
        if j > 0 :
            attackerDet = Attacker(nra + j + 1, 3 * j)
        else :
            attackerDet = Attacker(nra + j + 1, 2)
        print(f'ATTACKER_DET N.{nra + j + 1} SLEEP: {attackerDet.time_sleep}')
        threads_det.append(attackerDet)

# sys.exit(0)

for attacker, attackerDet in it.zip_longest(threads_att, threads_det):
    if attacker is not None :
        attacker.start()
    if attackerDet is not None :
        attackerDet.start()
        attackerDet.join()
    
for attacker in threads_att:
    attacker.join()

print("END")