import time
import mtd_ga_multi as mgm
import conf_manager as cm
import arbiter
from threading import Thread
from math import floor
from active_conf import activeConf

green = lambda text: '\033[0;32m' + text + '\033[0m'
cyan = lambda text: '\033[0;36m' + text + '\033[0m'

M = 12 # mutations
secLev = {"1": 3, "2": 2, "3": 1}
defense_types = {
    "token": 0, "ip": 1, "port": 2, "topology": 3, "mac": 4, "proxy": 5, 
    "web_server": 6, "operating_system": 7, "database": 8,
    "software_diversity": 9, "lm_vm": 10, "service_version": 11
}
# CODED DEFENSES: 1,6,7,8,10,11

# DEFENSE STRATEGY ENGINE
# Execute strategy selection with GA and apply the recovering plan
class ReconfigurationEngine(object):

    def __init__(self):
        self.activeConf = activeConf
    

    def evaluateMTD(self, sec_l, att_probs, att_awareness=None):
        conf = self.activeConf
        av_m = floor(M / secLev.get(f'{sec_l}'))
        print(f'Max number of mutations allowed: {av_m}')
        #budget = int(input("Enter your budget: "))
        n = conf.num_dcomponents
        num_vms = sum([ conf.dcomponents[i].type == "VM" for i in range(n) ])
        if att_awareness is None :
            return mgm.evaluate(av_m, num_vms, att_probs)
        else :
            return mgm.evaluate(av_m, num_vms, att_probs, att_awareness)
    

    def enforceMTD(self, strategies, enf_unit, target_id = None):
        conf = self.activeConf
        n = conf.num_dcomponents
        threads = []

        if strategies[1] == 1 :
            if target_id is None :
                print(green(">>>>>>>>> CHANGING IP >>>>>>>>>"))
                # enf_unit.changeIPs()
                t = Thread(target=enf_unit.changeIPs)
                t.start()
                threads.append(t)
            else :
                target_ip = None
                for i in range(n):
                    if conf.dcomponents[i].id == target_id :
                        target_ip = conf.dcomponents[i].ip_addr
                print(cyan(">>>>>>>>> CHANGING IP on component n.{} >>>>>>>>>".format(target_id)))
                # time.sleep(defense_durations.get("ip"))
                # enf_unit.changeIP(target_ip)
                t = Thread(target=enf_unit.changeIP, args=(target_ip,))
                t.start()
                threads.append(t)
        if strategies[6] == 1 :
            if target_id is None :
                print(green(">>>>>>>>> CHANGING WS >>>>>>>>>"))
                # time.sleep(defense_durations.get("web_server"))
                # enf_unit.changeWSrand()
                t = Thread(target=enf_unit.changeWSrand)
                t.start()
                threads.append(t)
            else :
                # n = self.activeConf.num_dcomponents
                # sel_name = None
                # sel_ws = []
                # for i in range(n):
                #     if self.activeConf.dcomponents[i].id == target_id :
                #         sel_name = self.activeConf.dcomponents[i].name
                # for i in range(n):
                #     if (self.activeConf.dcomponents[i].type == "WS" and self.activeConf.dcomponents[i].host == sel_name):
                #         sel_ws.append(self.activeConf.dcomponents[i].id)
                # for target_id in sel_ws:
                #     print(cyan(">>>>>>>>> CHANGING WS on component {} >>>>>>>>>".format(sel_name)))
                #     t = Thread(target=enf_unit.changeWS, args=(target_id,))
                #     t.start()
                #     threads.append(t)
                t = Thread(target=enf_unit.changeWS, args=(target_id,))
                t.start()
                threads.append(t)
        if strategies[7] == 1 :
            if target_id is None :
                print(green(">>>>>>>>> CHANGING OS >>>>>>>>>"))
                # time.sleep(defense_durations.get("operating_system"))
                # enf_unit.changeOSrand()
                t = Thread(target=enf_unit.changeOSrand)
                t.start()
                threads.append(t)
            else :
                print(cyan(">>>>>>>>> CHANGING OS on component n.{} >>>>>>>>>".format(target_id)))
                # time.sleep(defense_durations.get("operating_system"))
                # enf_unit.changeOS(target_id)
                t = Thread(target=enf_unit.changeOS, args=(target_id,))
                t.start()
                threads.append(t)
        if strategies[8] == 1 :
            if target_id is None :
                print(green(">>>>>>>>> CHANGING DBMS >>>>>>>>>"))
                # time.sleep(defense_durations.get("database"))
                # enf_unit.changeDBMSrand()
                t = Thread(target=enf_unit.changeDBMSrand)
                t.start()
                threads.append(t)
            else :
                # n = self.activeConf.num_dcomponents
                # sel_name = None
                # sel_dbms = []
                # for i in range(n):
                #     if self.activeConf.dcomponents[i].id == target_id :
                #         sel_name = self.activeConf.dcomponents[i].name
                # for i in range(n):
                #     if (self.activeConf.dcomponents[i].type == "DBMS" and self.activeConf.dcomponents[i].host == sel_name):
                #         sel_dbms.append(self.activeConf.dcomponents[i].id)
                # for target_id in sel_dbms:
                #     print(cyan(">>>>>>>>> CHANGING DBMS on component {} >>>>>>>>>".format(sel_name)))
                #     t = Thread(target=enf_unit.changeDBMS, args=(target_id,))
                #     t.start()
                #     threads.append(t)
                t = Thread(target=enf_unit.changeDBMS, args=(target_id,))
                t.start()
                threads.append(t)
        if strategies[9] == 1:
            if target_id is None :
                print(green(">>>>>>>>> CHANGING SOFTWARE >>>>>>>>>"))
                # time.sleep(defense_durations.get("software_diversity"))
                # enf_unit.changeSDrand()
                t = Thread(target=enf_unit.changeSDrand)
                t.start()
                threads.append(t)
            else :
                print(cyan(">>>>>>>>> CHANGING SOFTWARE on component n.{} >>>>>>>>>".format(target_id)))
                # time.sleep(defense_durations.get("software_diversity"))
                # enf_unit.changeSD(target_id)
                t = Thread(target=enf_unit.changeSD, args=(target_id,))
                t.start()
                threads.append(t)
        if strategies[10] == 1 :
            if target_id is None :
                print(green(">>>>>>>>> LM-VM >>>>>>>>>"))
                # time.sleep(defense_durations.get("lm_vm"))
                # enf_unit.changeVMrand()
                t = Thread(target=enf_unit.changeVMrand)
                t.start()
                threads.append(t)
            else :
                print(cyan(">>>>>>>>> LM-VM on component n.{} >>>>>>>>>".format(target_id)))
                # time.sleep(defense_durations.get("lm_vm"))
                # enf_unit.changeVM(target_id)
                t = Thread(target=enf_unit.changeVM, args=(target_id,))
                t.start()
                threads.append(t)
        if strategies[11] == 1 :
            if target_id is None :
                print(green(">>>>>>>>> CHANGING SERVICE VERSION >>>>>>>>>"))
                # time.sleep(defense_durations.get("service_version"))
                # enf_unit.changeSVrand()
                t = Thread(target=enf_unit.changeSVrand)
                t.start()
                threads.append(t)
            else :
                # target_type = None
                # target_product = None
                # for i in range(n):
                #     if conf.dcomponents[i].id == target_id :
                #         target_type = conf.dcomponents[i].type
                #         target_product = conf.dcomponents[i].product
                print(cyan(">>>>>>>>> CHANGING SERVICE VERSION on component n.{} >>>>>>>>>".format(target_id)))
                # time.sleep(defense_durations.get("service_version"))
                # enf_unit.changeSV(target_id)
                t = Thread(target=enf_unit.changeSV, args=(target_id,))
                t.start()
                threads.append(t)
                # enf_unit.changeSV(target_type, target_product, target_id)

        for t in threads:
            t.join()

