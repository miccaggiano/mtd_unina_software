import sys
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

def mean_absolute_percentage_error(y_true, y_pred):
        # y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

basepath = './results/graphs/'
#pd=.0 blue
#pd=.25 green 
#pd=.5 magenta 
MODE = 2 # MODE 0 --> graphs per combination ATT-DEF, MODE 1 --> graphs per probability of detection, MODE 2 --> graphs with subplots
GRAPH = 3

if MODE == 0 :
        x = np.array([72, 144, 288, 432, 576])
        if GRAPH == 0:
                #ATTACKER LOW - DEFENDER LOW
                ATT_LEV = "LOW"
                DEF_LEV = "LOW"
                y0 = np.array([12, 31, 60, 90, 128])
                y25 = np.array([12, 33, 63, 95, 132])
                y50 = np.array([14, 35, 70, 100, 136])

                regr = linear_model.LinearRegression()
                regr.fit(x.reshape(-1,1), y0)
                y0_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y25)
                y25_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y50)
                y50_preds = regr.predict(x.reshape(-1,1))

                vs0 = r2_score(y0, y0_preds)
                vs25 = r2_score(y25, y25_preds)
                vs50 = r2_score(y50, y50_preds)

                plt.title(f'Thwarted attacks vs exploited attacks \n \
                        Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
                plt.xlabel('Number of attacks')
                plt.ylabel('Number of thwarted attacks')
                plt.scatter(x, y0, color='dodgerblue')
                line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
                plt.scatter(x, y25, color='limegreen')
                line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
                plt.scatter(x, y50, color='violet')
                line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
                plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
                plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
                plt.grid()
                plt.tight_layout()
                plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
                # plt.show()
                # sys.exit(0)
        elif GRAPH == 1:
                #ATTACKER LOW - DEFENDER HIGH
                ATT_LEV = "LOW"
                DEF_LEV = "HIGH"
                y0 = np.array([22, 54, 117, 168, 210])
                y25 = np.array([28, 62, 126, 178, 236])
                y50 = np.array([30, 65, 132, 181, 274])

                regr = linear_model.LinearRegression()
                regr.fit(x.reshape(-1,1), y0)
                y0_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y25)
                y25_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y50)
                y50_preds = regr.predict(x.reshape(-1,1))

                vs0 = r2_score(y0, y0_preds)
                vs25 = r2_score(y25, y25_preds)
                vs50 = r2_score(y50, y50_preds)

                plt.title(f'Thwarted attacks vs exploited attacks \n \
                        Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
                plt.scatter(x, y0, color='dodgerblue')
                line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
                plt.scatter(x, y25, color='limegreen')
                line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
                plt.scatter(x, y50, color='violet')
                line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
                plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
                plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
                plt.grid()
                plt.tight_layout()
                plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
                # plt.show()
                # sys.exit(0)
        elif GRAPH == 2:
                #ATTACKER HIGH - DEFENDER LOW
                ATT_LEV = "HIGH"
                DEF_LEV = "LOW"
                y0 = np.array([15, 28, 55, 101, 120])
                y25 = np.array([15, 29, 60, 93, 125])
                y50 = np.array([15, 30, 65, 95, 130])

                regr = linear_model.LinearRegression()
                regr.fit(x.reshape(-1,1), y0)
                y0_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y25)
                y25_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y50)
                y50_preds = regr.predict(x.reshape(-1,1))

                vs0 = r2_score(y0, y0_preds)
                vs25 = r2_score(y25, y25_preds)
                vs50 = r2_score(y50, y50_preds)

                plt.title(f'Thwarted attacks vs exploited attacks \n \
                        Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
                plt.scatter(x, y0, color='dodgerblue')
                line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
                plt.scatter(x, y25, color='limegreen')
                line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
                plt.scatter(x, y50, color='violet')
                line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
                plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
                plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
                plt.grid()
                plt.tight_layout()
                plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
                # plt.show()
                # sys.exit(0)
        elif GRAPH == 3:
                #ATTACKER HIGH - DEFENDER HIGH
                ATT_LEV = "HIGH"
                DEF_LEV = "HIGH"
                y0 = np.array([20, 50, 105, 150, 200])
                y25 = np.array([25, 54, 110, 167, 220])
                y50 = np.array([28, 58, 115, 180, 231])

                regr = linear_model.LinearRegression()
                regr.fit(x.reshape(-1,1), y0)
                y0_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y25)
                y25_preds = regr.predict(x.reshape(-1,1))
                regr.fit(x.reshape(-1,1), y50)
                y50_preds = regr.predict(x.reshape(-1,1))

                vs0 = r2_score(y0, y0_preds)
                vs25 = r2_score(y25, y25_preds)
                vs50 = r2_score(y50, y50_preds)

                plt.title(f'Thwarted attacks vs exploited attacks \n \
                        Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
                plt.scatter(x, y0, color='dodgerblue')
                line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
                plt.scatter(x, y25, color='limegreen')
                line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
                plt.scatter(x, y50, color='violet')
                line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
                plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
                plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
                plt.grid()
                plt.tight_layout()
                plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
                # plt.show()
                # sys.exit(0)

elif MODE == 1 :
        # .0
        PD = 0
        x = np.array([72, 144, 288, 432, 576])
        yLL = np.array([12, 31, 60, 90, 128])
        yLH = np.array([12, 33, 63, 95, 132])
        yHL = np.array([14, 35, 70, 100, 136])
        yHH = np.array([14, 35, 70, 100, 136])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), yLL)
        yLL_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yLH)
        yLH_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yHL)
        yHL_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yHH)
        yHH_preds = regr.predict(x.reshape(-1,1))

        vsLL = r2_score(yLL, yLL_preds)
        vsLH = r2_score(yLH, yLH_preds)
        vsHL = r2_score(yHL, yHL_preds)
        vsHH = r2_score(yHH, yHH_preds)

        plt.title(f'Thwarted attacks vs exploited attacks \n \
                Probability of detection = .{PD}')
        plt.xlabel('Number of attacks')
        plt.ylabel('Number of thwarted attacks')
        plt.scatter(x, yLL, color='salmon')
        lineLL, = plt.plot(x, yLL_preds, color='darkred', linewidth='3')
        plt.scatter(x, yLH, color='orange')
        lineLH, = plt.plot(x, yLH_preds, color='darkorange', linewidth='3')
        plt.scatter(x, yHL, color='yellowgreen')
        lineHL, = plt.plot(x, yHL_preds, color='olive', linewidth='3')
        plt.scatter(x, yHH, color='deepskyblue')
        lineHH, = plt.plot(x, yHH_preds, color='royalblue', linewidth='3')
        plt.legend([lineLL, lineLH, lineHL, lineHH], ['ATT = LOW, DEF = LOW', 'ATT = LOW, DEF = HIGH', 'ATT = HIGH, DEF = LOW', 'ATT = HIGH, DEF = HIGH'])
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        plt.grid()
        plt.tight_layout()
        # plt.savefig(basepath + f'comb_{PD}.png')
        plt.show()
        # sys.exit(0)

        # .25
        PD = 25
        yLL = np.array([])
        yLH = np.array([])
        yHL = np.array([])
        yHH = np.array([])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), yLL)
        yLL_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yLH)
        yLH_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yHL)
        yHL_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yHH)
        yHH_preds = regr.predict(x.reshape(-1,1))

        vsLL = r2_score(yLL, yLL_preds)
        vsLH = r2_score(yLH, yLH_preds)
        vsHL = r2_score(yHL, yHL_preds)
        vsHH = r2_score(yHH, yHH_preds)

        plt.title(f'Thwarted attacks vs exploited attacks \n \
                Probability of detection = .{PD}')
        plt.xlabel('Number of attacks')
        plt.ylabel('Number of thwarted attacks')
        plt.scatter(x, yLL, color='salmon')
        lineLL, = plt.plot(x, yLL_preds, color='darkred', linewidth='3')
        plt.scatter(x, yLH, color='orange')
        lineLH, = plt.plot(x, yLH_preds, color='darkorange', linewidth='3')
        plt.scatter(x, yHL, color='yellowgreen')
        lineHL, = plt.plot(x, yHL_preds, color='olive', linewidth='3')
        plt.scatter(x, yHH, color='deepskyblue')
        lineHH, = plt.plot(x, yHH_preds, color='royalblue', linewidth='3')
        plt.legend([lineLL, lineLH, lineHL, lineHH], ['ATT = LOW, DEF = LOW', 'ATT = LOW, DEF = HIGH', 'ATT = HIGH, DEF = LOW', 'ATT = HIGH, DEF = HIGH'])
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        plt.grid()
        plt.tight_layout()
        # plt.savefig(basepath + f'comb_{PD}.png')
        plt.show()
        # sys.exit(0)

        # .5
        PD = 50
        yLL = np.array([])
        yLH = np.array([])
        yHL = np.array([])
        yHH = np.array([])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), yLL)
        yLL_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yLH)
        yLH_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yHL)
        yHL_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), yHH)
        yHH_preds = regr.predict(x.reshape(-1,1))

        vsLL = r2_score(yLL, yLL_preds)
        vsLH = r2_score(yLH, yLH_preds)
        vsHL = r2_score(yHL, yHL_preds)
        vsHH = r2_score(yHH, yHH_preds)

        plt.title(f'Thwarted attacks vs exploited attacks \n \
                Probability of detection = .{PD}')
        plt.xlabel('Number of attacks')
        plt.ylabel('Number of thwarted attacks')
        plt.scatter(x, yLL, color='salmon')
        lineLL, = plt.plot(x, yLL_preds, color='darkred', linewidth='3')
        plt.scatter(x, yLH, color='orange')
        lineLH, = plt.plot(x, yLH_preds, color='darkorange', linewidth='3')
        plt.scatter(x, yHL, color='yellowgreen')
        lineHL, = plt.plot(x, yHL_preds, color='olive', linewidth='3')
        plt.scatter(x, yHH, color='deepskyblue')
        lineHH, = plt.plot(x, yHH_preds, color='royalblue', linewidth='3')
        plt.legend([lineLL, lineLH, lineHL, lineHH], ['ATT = LOW, DEF = LOW', 'ATT = LOW, DEF = HIGH', 'ATT = HIGH, DEF = LOW', 'ATT = HIGH, DEF = HIGH'])
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        plt.grid()
        plt.tight_layout()
        # plt.savefig(basepath + f'comb_{PD}.png')
        plt.show()
        # sys.exit(0)
elif MODE == 2:
        plt.figure(figsize=(18,12))
        plt.suptitle('Thwarted attacks vs exploited attacks', fontsize=16)
        x = np.array([72, 144, 288, 432, 576])

        #ATTACKER LOW - DEFENDER LOW
        plt.subplot(221)
        ATT_LEV = "LOW"
        DEF_LEV = "LOW"
        y0 = np.array([12, 31, 60, 90, 128])
        y25 = np.array([12, 33, 63, 95, 132])
        y50 = np.array([14, 35, 70, 100, 136])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}', fontsize=14)
        plt.ylabel('Number of thwarted attacks')
        plt.ylim(top=300)
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        # print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        # print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        # Plot the residuals after fitting a linear model
        # sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        # sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        # sns.residplot(x, y50 - y50_preds, lowess=True, color='m')
        plt.grid()
        # plt.tight_layout()
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        # plt.show()
        # sys.exit(0)

        #ATTACKER LOW - DEFENDER HIGH
        plt.subplot(222)
        ATT_LEV = "LOW"
        DEF_LEV = "HIGH"
        y0 = np.array([22, 54, 117, 168, 210])
        y25 = np.array([28, 62, 126, 178, 236])
        y50 = np.array([30, 65, 132, 181, 274])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}', fontsize=14)
        plt.ylim(top=300)
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # Plot the residuals after fitting a linear model
        # sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        # sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        # sns.residplot(x, y50 - y50_preds, lowess=True, color='m')
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        # print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        # print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        plt.grid()
        # plt.tight_layout()
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        # plt.show()
        # sys.exit(0)

        #ATTACKER HIGH - DEFENDER LOW
        plt.subplot(223)
        ATT_LEV = "HIGH"
        DEF_LEV = "LOW"
        y0 = np.array([15, 28, 55, 101, 120])
        y25 = np.array([15, 29, 60, 93, 125])
        y50 = np.array([15, 30, 65, 95, 130])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}', fontsize=14)
        plt.ylabel('Number of thwarted attacks')
        plt.xlabel('Number of attacks')
        plt.ylim(top=300)
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # Plot the residuals after fitting a linear model
        # sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        # sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        # sns.residplot(x, y50 - y50_preds, lowess=True, color='m')
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        # print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        # print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        plt.grid()
        # plt.tight_layout()
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        # plt.show()
        # sys.exit(0)

        #ATTACKER HIGH - DEFENDER HIGH
        plt.subplot(224)
        ATT_LEV = "HIGH"
        DEF_LEV = "HIGH"
        y0 = np.array([20, 50, 105, 150, 200])
        y25 = np.array([25, 54, 110, 167, 220])
        y50 = np.array([28, 58, 115, 180, 231])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}', fontsize=14)
        plt.xlabel('Number of attacks')
        plt.ylim(top=300)
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # Plot the residuals after fitting a linear model
        # sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        # sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        # sns.residplot(x, y50 - y50_preds, lowess=True, color='m')
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        # print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        # print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        plt.grid()
        # plt.tight_layout()
        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        # plt.show()
        plt.savefig(basepath + f'Figure_subplots.png')
        # sys.exit(0)
elif MODE == 3:
        plt.figure(figsize=(18,12))
        plt.suptitle('Thwarted attacks vs exploited attacks')
        x = np.array([72, 144, 288, 432, 576])

        #ATTACKER LOW - DEFENDER LOW
        plt.subplot(221)
        ATT_LEV = "LOW"
        DEF_LEV = "LOW"
        y0 = np.array([15, 31, 63, 94, 128])
        y25 = np.array([12, 33, 63, 93, 136])
        y50 = np.array([14, 30, 70, 94, 132])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)
        mape0 = mean_absolute_percentage_error(y0, y0_preds)
        mape25 = mean_absolute_percentage_error(y25, y25_preds)
        mape50 = mean_absolute_percentage_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.ylabel('Number of thwarted attacks')
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # Plot the residuals after fitting a linear model
        sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        sns.residplot(x, y50 - y50_preds, lowess=True, color='m')
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        print(f'Mean Absolute Percentage Error score: {mape0:.2f}, {mape25:.2f}, {mape50:.2f}')
        plt.grid()
        # plt.tight_layout()
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        # plt.show()
        # sys.exit(0)

        #ATTACKER LOW - DEFENDER HIGH
        plt.subplot(222)
        ATT_LEV = "LOW"
        DEF_LEV = "HIGH"
        y0 = np.array([25, 54, 117, 168, 230])
        y25 = np.array([25, 62, 126, 173, 236])
        y50 = np.array([22, 55, 132, 181, 214])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)
        mape0 = mean_absolute_percentage_error(y0, y0_preds)
        mape25 = mean_absolute_percentage_error(y25, y25_preds)
        mape50 = mean_absolute_percentage_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # Plot the residuals after fitting a linear model
        sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        sns.residplot(x, y50 - y50_preds, lowess=True, color='m')
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        print(f'Mean Absolute Percentage Error score: {mape0:.2f}, {mape25:.2f}, {mape50:.2f}')
        plt.grid()
        # plt.tight_layout()
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        # plt.show()
        # sys.exit(0)

        #ATTACKER HIGH - DEFENDER LOW
        plt.subplot(223)
        ATT_LEV = "HIGH"
        DEF_LEV = "LOW"
        y0 = np.array([15, 28, 60, 101, 125])
        y25 = np.array([15, 29, 60, 93, 125])
        y50 = np.array([15, 30, 65, 95, 121])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)
        mape0 = mean_absolute_percentage_error(y0, y0_preds)
        mape25 = mean_absolute_percentage_error(y25, y25_preds)
        mape50 = mean_absolute_percentage_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.ylabel('Number of thwarted attacks')
        plt.xlabel('Number of attacks')
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # Plot the residuals after fitting a linear model
        sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        sns.residplot(x, y50 - y50_preds, lowess=True, color='m')
        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        # print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        # print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        # print(f'Mean Absolute Percentage Error score: {mape0:.2f}, {mape25:.2f}, {mape50:.2f}')
        plt.grid()
        plt.tight_layout()
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        # plt.show()
        # sys.exit(0)

        #ATTACKER HIGH - DEFENDER HIGH
        plt.subplot(224)
        ATT_LEV = "HIGH"
        DEF_LEV = "HIGH"
        y0 = np.array([32, 50, 105, 180, 225])
        y25 = np.array([22, 54, 115, 167, 230])
        y50 = np.array([34, 41, 115, 150, 231])

        regr = linear_model.LinearRegression()
        regr.fit(x.reshape(-1,1), y0)
        y0_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y25)
        y25_preds = regr.predict(x.reshape(-1,1))
        regr.fit(x.reshape(-1,1), y50)
        y50_preds = regr.predict(x.reshape(-1,1))

        vs0 = r2_score(y0, y0_preds)
        vs25 = r2_score(y25, y25_preds)
        vs50 = r2_score(y50, y50_preds)
        mse0 = mean_squared_error(y0, y0_preds)
        mse25 = mean_squared_error(y25, y25_preds)
        mse50 = mean_squared_error(y50, y50_preds)
        mape0 = mean_absolute_percentage_error(y0, y0_preds)
        mape25 = mean_absolute_percentage_error(y25, y25_preds)
        mape50 = mean_absolute_percentage_error(y50, y50_preds)

        # plt.title(f'Thwarted attacks vs exploited attacks \n \
        #         Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.title(f'Attacker skills = {ATT_LEV}, defender skills = {DEF_LEV}')
        plt.xlabel('Number of attacks')
        plt.scatter(x, y0, color='dodgerblue')
        line0, = plt.plot(x, y0_preds, color='blue', linewidth='3')
        plt.scatter(x, y25, color='limegreen')
        line25, = plt.plot(x, y25_preds, color='forestgreen', linewidth='3')
        plt.scatter(x, y50, color='violet')
        line50, = plt.plot(x, y50_preds, color='magenta', linewidth='3')
        plt.legend([line0, line25, line50], ['pd = .0', 'pd = .25', 'pd = .5'])
        # Plot the residuals after fitting a linear model
        sns.residplot(x, y0 - y0_preds, lowess=True, color='b')
        sns.residplot(x, y25 - y25_preds, lowess=True, color='g')
        sns.residplot(x, y50 - y50_preds, lowess=True, color='m')

        # plt.figtext(0.99, 0.01,  f'Variance score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}', horizontalalignment='right')
        print(f'R^2 score: {vs0:.2f}, {vs25:.2f}, {vs50:.2f}')
        print(f'Mean Squared Error score: {mse0:.2f}, {mse25:.2f}, {mse50:.2f}')
        print(f'Mean Absolute Percentage Error score: {mape0:.2f}, {mape25:.2f}, {mape50:.2f}')
        plt.grid()
        plt.tight_layout()
        # plt.savefig(basepath + f'{ATT_LEV}_{DEF_LEV}.png')
        plt.show()
        # sys.exit(0)
