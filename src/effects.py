import json
import numpy as np

score_dict = {
    "L": 1,
    "M": 2,
    "H": 3
}

# read JSON files of application deployment configurations
def read_conf(conf_id) :
	with open(conf_id) as f:
		data = json.load(f)
	return data

# Retrieve attack damages and defense effectiveness from JSON file
def evaluateEff(fileEff):
    eff = read_conf(fileEff)
    AM = []
    list_att = ['scan', 'ddos', 'csrf', 'redirect', 'xss', \
                'sql_injection', 'os_injection', 'trojan', \
                'worm', 'rootkit', 'buffer_overflow', \
                'jamming', 'side_channel']
    list_def = ['token', 'ip', 'port', 'topology', 'MAC', \
                'proxy', 'web_server', 'operating_system', \
                'database', 'software_diversity', 'LM-VM', \
                'service_version']
    N = len(list_att)
    D = len(list_def)

    matrix = np.zeros((D, N))

    for i in list_att:
        AM.append((i, score_dict.get(eff[i]['AM'])))
        for j in list_def:
            if j in eff[i]['L']:
                matrix[list_def.index(j)][list_att.index(i)] = score_dict.get('L')
            elif j in eff[i]['M']:
                matrix[list_def.index(j)][list_att.index(i)] = score_dict.get('M')
            elif j in eff[i]['H']:
                matrix[list_def.index(j)][list_att.index(i)] = score_dict.get('H')
            else:
                print("{}: AFAMMOC".format(j))
    
    #print(AM)
    #print(matrix)
    return (matrix, AM)


if __name__ == "__main__" :
	evaluateEff('./ahp/effects.json')