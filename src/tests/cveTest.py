import json
import os
import numpy as np
#  Python wrapper of CIRCL cve-search REST APIs (github)
from pycvesearch import CVESearch
from threading import Thread
from progressbar import progressbar

bold = lambda text: '\033[0;1m' + text + '\033[0m'
yellow = lambda text: '\033[0;33m' + text + '\033[0m'
listP_a = ["amazon_aws", "cloud_platform", "sqlite", "tomcat", "mariadb",
    "postgresql", "nginx", "internet_information_server", "mysql"]
listP_o = ["azure_devops_server", "windows_server_2016",
    "ubuntu_linux", "enterprise_linux", "freebsd", "solaris"]


class SecurityAssessmentUnit(object):

    def __init__(self):
        self.basepath = './docs/'
        self.secThreshold = 6.9
        self.max_score = 0
        self.activeConf = None
        self.alive = True

    # SETUP STAGE - STATIC SECURITY ASSESSMENT
    # find the initial application deployment configuration to deploy
    # evaluate  average security risk score as average CVSS score of
    # CVE vulnerabilities found in CPE dictionary filtered by CPE 2.3 id
    # on total number of components deployed into configuration.
    def secAssessment(self):
        cve = CVESearch()

        conf_gen = None
        with open('./conf_gen_cve.json') as f:
            conf_gen = json.load(f)
        # conf_gen = read_conf('./conf_gen.json')

        n_dbms = conf_gen['num_dbms']
        print(f'n_DBMS: {n_dbms}')
        n_csp = conf_gen['num_csp']
        print(f'n_CSP: {n_csp}')
        n_ws = conf_gen['num_ws']
        print(f'n_WS: {n_ws}')
        n_vm = conf_gen['num_vm']
        print(f'n_VM: {n_vm}')

        dbms_vendors = [conf_gen['dbms'][j]['vendor'] for j in range(n_dbms)]
        dbms_products = [conf_gen['dbms'][j]['product'] for j in range(n_dbms)]
        dbms_versions = [conf_gen['dbms'][j]['version'] for j in range(n_dbms)]
        csp_vendors = [conf_gen['csp'][j]['vendor'] for j in range(n_csp)]
        csp_products = [conf_gen['csp'][j]['product'] for j in range(n_csp)]
        csp_versions = [conf_gen['csp'][j]['version'] for j in range(n_csp)]
        ws_vendors = [conf_gen['ws'][j]['vendor'] for j in range(n_ws)]
        ws_products = [conf_gen['ws'][j]['product'] for j in range(n_ws)]
        ws_versions = [conf_gen['ws'][j]['version'] for j in range(n_ws)]
        vm_vendors = [conf_gen['vm'][j]['vendor'] for j in range(n_vm)]
        vm_products = [conf_gen['vm'][j]['product'] for j in range(n_vm)]
        vm_versions = [conf_gen['vm'][j]['version'] for j in range(n_vm)]

        scores_all = []
        threads_all = []

        for j in range(n_dbms):
            def ss_dbms():
                tmp_scores = 0
                search = 'cpe:2.3:a:' + '{}'.format(dbms_vendors[j]) + ':{}'.format(
                    dbms_products[j]) + ':{}'.format(dbms_versions[j]) + ':*'
                print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(bold("%s --> %.1f" % (search, tmp_scores)))
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                    print(bold(f'Failed to lookup: {search}'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_dbms)
            threads_all.append(t)
            t.start()

        for j in range(n_csp):
            def ss_csp():
                tmp_scores = 0
                # amazon a, google a, microsoft ws16 o, mysql a, canonical o, apache a
                if csp_products[j] in ["amazon_aws", "cloud_platform"] :
                    search = 'cpe:2.3:a:' + '{}'.format(csp_vendors[j]) + ':{}'.format(csp_products[j]) + ':{}'.format(csp_versions[j]) + ':*'
                elif csp_products[j] in ["azure_devops_server"]:
                    search = 'cpe:2.3:o:' + '{}'.format(csp_vendors[j]) + ':{}'.format(csp_products[j]) + ':{}'.format(csp_versions[j]) + ':*'
                print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(bold("%s --> %.1f" % (search, tmp_scores)))
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                    print(bold(f'Failed to lookup: {search}'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_csp)
            threads_all.append(t)
            t.start()

        for j in range(n_ws):
            def ss_ws():
                tmp_scores = 0
                search = 'cpe:2.3:a:' + '{}'.format(ws_vendors[j]) + ':{}'.format(ws_products[j]) + ':{}'.format(ws_versions[j]) + ':*'
                print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(bold("%s --> %.1f" % (search, tmp_scores)))
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                	print(bold(f'Failed to lookup: {search}'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_ws)
            threads_all.append(t)
            t.start()

        for j in range(len(vm_vendors)):
            def ss_vm():
                tmp_scores = 0
                search = 'cpe:2.3:o:' + '{}'.format(vm_vendors[j]) + ':{}'.format(vm_products[j]) + ':{}'.format(vm_versions[j]) + ':*'
                print(search)
                try:
                    res = cve.cvefor(search)
                    tmp_scores += np.sum(np.asarray([ res[k].get('cvss') for k in range(len(res)) ], dtype='float'))
                    print(bold("%s --> %.1f" % (search, tmp_scores)))
                    if len(res) > 0 :
                        tmp_scores = tmp_scores / len(res)
                except ValueError:
                	print(bold(f'Failed to lookup: {search}'))
                scores_all.append( (search, tmp_scores) )
            t = Thread(target=ss_vm)
            threads_all.append(t)
            t.start()
        
        for t in threads_all:
            t.join()
        
        threads_all.clear()

if __name__ == "__main__":
    su = SecurityAssessmentUnit()
    su.secAssessment()
