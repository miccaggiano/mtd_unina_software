import pandas as pd
import matplotlib.pyplot as plt
import pickle as pk
import numpy as np
import seaborn as sns
import scipy.optimize as sciopt
from scipy import stats
#from scipy.stats import binom
#from scipy.stats import poisson
import os, sys
#from statsmodels.distributions.empirical_distribution import ECDF

pink = lambda text: '\033[0;95m' + text + '\033[0m'
MODE = 0
NUM_RANDOM_ATTACKS = 750
NUM_ATTACKS = 341
attack_types = {"scan": 0, "ddos": 1, "csrf": 2, "redirect": 3, "xss": 4, "sql_injection": 5, "os_injection": 6,
                "trojan": 7, "worm": 8, "rootkit": 9, "buffer_overflow": 10, "jamming": 11, "side_channel": 12}

# coda delle previsioni sugli attacchi (detected e non) dell'arbitro
queue_predictions = pk.load(open('../tmp_pred.txt', 'rb'))
# coda dei risultati effettivi riportati dall'arbitro
queue_scanAtt = pk.load(open('../tmp_att.txt', 'rb'))
# coda degli attacchi: (1, False) -> atta
# queue_seqAttacks = pk.load(open('../tmp_seqAtt.txt', 'rb'))

def fitFunc1(x, a1, a2, a3, b1, b2, b3, c1, c2, c3):
	return (a1 * np.exp(-((x - b1) / c1)**2) + a2 * np.exp(-((x - b2) / c2)**2) + a3 * np.exp(-((x - b3) / c3)**2))
	
def fitFunc(x, a, b, c):
    return (a * np.exp(-b * x) + c)

def ecdf(sample):
	# convert sample to a numpy array, if it isn't already
	sample = np.atleast_1d(sample)
	# find the unique values and their corresponding counts
	quantiles, counts = np.unique(sample, return_counts=True)
	# take the cumulative sum of the counts and divide by the sample size to
	# get the cumulative probabilities between 0 and 1
	cumprob = np.cumsum(counts).astype(np.double) / sample.size
	return quantiles, cumprob


def printEffectiveness():
	tot_attempts = NUM_ATTACKS
	n_p = len(queue_predictions)
	n_r = len(queue_scanAtt)
	n_succ_p = 0
	n_succ_att = 0
	
	rate_succ = pk.load(open('./tmp_parTest.txt', 'rb'))
	#print(rate_succ)
	
	eff_res = [ [] for _ in range(len(attack_types)) ]
	if n_p == n_r :
		for i in range(len(attack_types)):
			n_inP = len(queue_predictions[i])
			n_inR = len(queue_scanAtt[i])
			n_succ_p += sum(queue_predictions[i])
			n_succ_att += sum(queue_scanAtt[i])
			if n_inP == n_inR :
				for j in range(n_inP):
					if queue_predictions[i][j] - queue_scanAtt[i][j] == 1 :
						eff_res[i].append(1)
					else :
						eff_res[i].append(0)
		
		par_rate_succ = [ [] for i in range(len(attack_types)) ]
		for k in range(len(attack_types)) :
			#n_attempts = sum(self.num_attempts[k])
			for h in range(len(eff_res[k])) :
				par_sum = sum(eff_res[k][:h])
				par_rate_succ[k].append(par_sum / (h + 1))

	att_types = list(attack_types.keys())

	par_rate_succ_stats = {
		"types": att_types,
		"par_rate_succ": par_rate_succ
	}
	
	with open('./tmp_effTest.txt', 'wb') as f :
		pk.dump(par_rate_succ_stats, f, pk.HIGHEST_PROTOCOL)
	
	data1 = pd.DataFrame(par_rate_succ_stats)

	print(pink('------------------------------------------------------------'))
	print(pink(f'TOTAL NUMBER OF ATTACKS: {tot_attempts}'))
	print(pink(f'NUMBER OF ATTACKS (REC + EXP) PREDICTED AS SUCCESSFULL: {n_succ_p}'))
	print(pink(f'NUMBER OF ATTACKS (REC + EXP) VERIFIED AS SUCCESSFULL: {n_succ_att}'))
	print(pink(f'NUMBER OF ATTACKS (SCAN) SUCCESSFULLY THWARTED: {sum(eff_res[0])}'))
	print(pink(f'NUMBER OF ATTACKS (NOT SCAN) SUCCESSFULLY THWARTED: {sum([ sum(eff_res[i]) for i in range(1, len(eff_res)) ])}'))

	# GRAPHICS
	n_fig = len(os.listdir('../results/'))

	if MODE == 0 :
		n_fig = 0
		for i in range(len(attack_types)):
			sec_lev = 2
			NUM_DETECTED_ATTACKS = 120
			# n_fig = len(os.listdir(self.basepath + "/graphs/"))
			at = list(attack_types.keys())[i]
			n = 0
			if i == 0:
				n = NUM_ATTACKS
			else:
				n = NUM_ATTACKS / (len(attack_types) - 1)
			#p = par_rate_succ_stats['par_rate_succ'][i]
			p = rate_succ['par_rate_succ'][i]
			# compute the ECDF of the samples
			#qe, pe = ecdf(p)
			#f = stats.binom(n, np.max(p))
			# evaluate the theoretical CDF over the same range
			#q = np.linspace(qe[0], qe[-1], n)
			#pf = f.cdf(q)
			fig, ax = plt.subplots(1, 1)
			ax.grid()
			#ax.plot(q, pf, '-k', lw=2, label='Theoretical CDF')
			#ax.plot(qe, pe, '-r', lw=2, label='Empirical CDF')
			#ax.set_xlabel('Quantile')
			#ax.set_ylabel('Cumulative probability')
			#ax.legend(fancybox=True, loc='right')
			print(p)
			x = np.linspace(1, n, n)
			#x = np.arange(poisson.ppf(0.01, p), poisson.ppf(0.99, p))
			pmf = stats.poisson.pmf(x, p, loc=n)
			cdf = stats.poisson.pmf(x, p, loc=n)
			ax.plot(x, cdf)
			#cdf = poisson.cdf(x, p)
			#pdf = binom.pmf(x, n, p)
			#cdf = binom.cdf(x, n, p)
			#fig, axs = plt.subplots(2)
			#fig.suptitle(f'MTD-enabled success rate\nSec_lev = {sec_lev}, Total Attacks = {NUM_ATTACKS}, Detected Attacks = {NUM_DETECTED_ATTACKS}', fontsize=12)
			#axs[0].grid()
			#axs[0].plot(x, pmf, 'bo')
			#axs[0].set_title(f'pmf - {at}')
			#axs[0].set_xlabel('')
			#axs[0].set_ylabel('Success probability')
			#axs[0].set_xlim(right=n)
			#axs[1].grid()
			#axs[1].plot(x, cdf, 'r-')
			#axs[1].set_title(f'cdf - {at}')
			#axs[1].set_xlabel('Number of attacks')
			#axs[1].set_ylabel('Success rate')
			#axs[1].set_xlim(right=n)
			#fig.tight_layout(rect=[0, 0.03, 1, 0.9])
			fig.tight_layout()
			plt.show()
			n_fig += 1
			sys.exit(0)
	elif MODE == 1 :
		sns.set(color_codes=True)
		sns.set_style('whitegrid')
		fig = plt.figure(figsize=(10, 10))
		# Plot effectiveness in terms of thwarted scans
		plt.subplot(211)
		plt.title('MTD effectiveness')
		data = data1['par_rate_succ'][0]
		x = np.linspace(1, tot_attempts, num=tot_attempts, dtype=int)
		plt.xlabel('Number of attacks')
		plt.ylabel('Success rate - reconnaissance')
		f = np.poly1d(np.polyfit(x, data, 4))
		
		plt.ylim(top=1)
		plt.xlim(right=tot_attempts)

		# Fitting with gaussian
		# popt1, pcov1 = sciopt.curve_fit(fitFunc1, x[:250], data[:250])
		# popt, pcov = sciopt.curve_fit(fitFunc1, x[:300], data[:300], p0=(0.4528, 0.4167, 0.4095, 0.3992, 106, 12, 182, 273, 51.2645, 75.1058, 30.1370, 21.8752))

		x_1 = np.linspace(300, tot_attempts, num=tot_attempts-300, dtype=int)
		# Fitting with exponential
		popt2, pcov2 = sciopt.curve_fit(fitFunc, x_1, data[300:], p0=(1, 1e-3, 1))
		# print(pink(f'POPT = {popt}'))
		# print(pink(f'PCOV = {pcov}'))
		
		# plt.plot(x, data, 'o', x, fitFunc(x, *popt2), '-', x[:250], fitFunc1(x[:250], *popt1), 'g-')
		plt.plot(x, data, 'o', x_1, fitFunc(x, *popt2), '-')
		
		# plt.plot(x, data, 'o', x, f(x), '-')
		
		# Plot effectiveness per attack type
		plt.subplot(212)
		sns.boxplot(data=data1['par_rate_succ'])
		x1 = np.linspace(0, len(data1['types']), len(data1['types']))
		plt.xticks(x1, labels=data1['types'], rotation=45, horizontalalignment='left')
		plt.ylabel('Distribution of success rate')
		plt.xlabel('Attack types')
		plt.ylim(top=1)
		
		# print("N_FIG : {}".format(n_fig))
		fig.tight_layout()
		# fig.savefig("../results/Figure_{}.png".format(n_fig + 1))
		plt.show()
		
		# Print r-squared error and SSE
		# modelPredictions = fitFunc1(x, *popt1)
		# absError = modelPredictions - data
		# SE = np.square(absError) # squared errors
		# MSE = np.mean(SE) # mean squared errors
		# RMSE = np.sqrt(MSE) # Root Mean Squared Error, RMSE
		# Rsquared = 1.0 - (np.var(absError) / np.var(data))
		# print(f'SSE : {np.sum(SE)}')
		# print(f'RMSE: {RMSE}')
		# print(f'R-squared: {Rsquared}')
		
		# Using fitter
		# f = fitter.Fitter(data, xmin=0, xmax=1, timeout=20)
		# f.fit()
		# f.summary()
	elif MODE == 2 :
		sns.set(color_codes=True)
		sns.set_style('whitegrid')
		fig = plt.figure(figsize=(10, 10))
		# Plot effectiveness in terms of thwarted scans
		plt.subplot(211)
		plt.title('MTD effectiveness')
		data = data1['par_rate_succ'][0]
		x = np.linspace(1, tot_attempts, num=tot_attempts, dtype=int)
		plt.xlabel('Number of attacks')
		plt.ylabel('Success rate - reconnaissance')
		#f = np.poly1d(np.polyfit(x, data, 4))
		

		# Fitting with gaussian
		popt1, pcov1 = sciopt.curve_fit(fitFunc1, x[:250], data[:250])
		# popt, pcov = sciopt.curve_fit(fitFunc1, x[:300], data[:300], p0=(0.4528, 0.4167, 0.4095, 0.3992, 106, 12, 182, 273, 51.2645, 75.1058, 30.1370, 21.8752))

		# Fitting with exponential
		popt2, pcov2 = sciopt.curve_fit(fitFunc, x[250:], data[250:], p0=(1, 1e-3))
		# print(pink(f'POPT = {popt}'))
		# print(pink(f'PCOV = {pcov}'))

		plt.plot(x, data, 'o', x, fitFunc(x, *popt2), '-', x[:250], fitFunc1(x[:250], *popt1), 'g-')
		# plt.plot(x, data, 'o', x, fitFunc1(x, *popt), '-')
		
		# plt.plot(x, data, 'o', x, f(x), '-')
		plt.ylim(top=1)
		plt.xlim(right=tot_attempts)
		
		# Plot effectiveness per attack type
		plt.subplot(212)
		sns.boxplot(data=data1['par_rate_succ'])
		x = np.linspace(0, len(data1['types']), len(data1['types']))
		plt.xticks(x, labels=data1['types'], rotation=45, horizontalalignment='left')
		plt.ylabel('Distribution of success rate')
		plt.xlabel('Attack types')
		plt.ylim(top=1)
		
		# print("N_FIG : {}".format(n_fig))
		fig.tight_layout()
		# fig.savefig("../results/Figure_{}.png".format(n_fig + 1))
		plt.show()
		


if __name__ == '__main__' :
	printEffectiveness()
