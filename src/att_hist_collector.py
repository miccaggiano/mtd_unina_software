import threading, queue
import proactive_unit as pu

yellow_und = lambda text: '\033[2;33m' + text + '\033[0m'


class AttackHistoryCollector(object):

    def __init__(self, num_att):
        self.num_att_type = num_att
        self.det_att = [ 0 for i in range(self.num_att_type) ]
        self.hist_data = [90, 4, 12, 11, 7, 10, 9, 9, 6, 10, 6, 3, 3]
        self.num_attacks = 90
        self.num_scans = 90
        #self.att_probs = [.09, .070, .077, .077, .077, .077, .077, .077, .077, .077, .077, .071, .076]
        self.att_probs = [ self.hist_data[i] / self.num_attacks for i in range(self.num_att_type) ]
    

    def update(self, att_idx):
        self.det_att[att_idx] += 1
        if att_idx == 0 :
            pa = self.att_probs[att_idx] * self.num_scans
            self.num_scans += 1
            pa += 1
            self.att_probs[att_idx] = pa / self.num_scans
        else :
            pa = self.att_probs[att_idx] * self.num_attacks
            self.num_attacks += 1
            pa += 1
            self.att_probs[att_idx] = pa / self.num_attacks


    def getProbs(self):
        print(yellow_und(f'Attack probability at current time: {self.att_probs}'))
        return self.att_probs


class AlertManager(object):

    def __init__(self, confM, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cm = confM
        self.queue_ref = pu.att_alerted
        self.alive = True
    

    def alert(self, att_id, att_type, target_ip, target_cve):
        if self.alive is True :
            self.queue_ref.put_nowait((att_id, att_type, target_ip, target_cve))
            print(yellow_und(f'----------------- ATTACK {target_cve} DETECTED -----------------'))
    

    def stop(self):
        self.alive = False
        print(yellow_und("----------------- ALERT MANAGER STOPPED -----------------"))

