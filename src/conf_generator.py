import json
import cli_ui
import sys
import math
import re
import itertools as it
from progressbar import progressbar


class ConfigurationGenerator(object):

	def __init__(self):
		self.basepath = './docs/'
		self.file_gen = "./conf_gen.json"


	# read JSON files of application deployment configurations
	def read_conf(self, conf_id) :
		with open(conf_id) as f:
			data = json.load(f)
		return data


	# write JSON files of application deployment configurations
	def write_conf(self, conf_toWrite):
		with open(self.basepath + 'conf_{}.json'.format(conf_toWrite['id']), 'w') as f:
			json.dump(conf_toWrite, f)


	# CONFIGURATION GENERATION
	# User-interactive generation of JSON files describing application 
	# deployment configrations to select the first one to deploy
	def apply(self, n_db, n_wa, n_s, vm_min):
		conf_gen = self.read_conf(self.file_gen)

		comps = n_db + n_wa + n_s
		print("DBs: {}, WebApps: {}, Services: {}".format(n_db, n_wa, n_s))
		
		n_dbms = conf_gen['num_dbms']
		print(f'n_DBMS: {n_dbms}')
		n_csp = conf_gen['num_csp']
		print(f'n_CSP: {n_csp}')
		n_ws = conf_gen['num_ws']
		print(f'n_WS: {n_ws}')
		n_vm = conf_gen['num_vm']
		print(f'n_VM: {n_vm}')

		dbms = range(n_dbms)
		ws = range(n_ws)
		vm = range(n_vm)
		csp = range(n_csp)

		dep_dbms = it.product(dbms, vm, csp)
		dep_ws = it.product(ws, vm, csp)
		dep_vm = it.product(vm, csp)

		dep_db = it.product(dep_dbms, repeat=n_db)
		dep_wa = it.product(dep_ws, repeat=n_wa)
		dep_s = it.product(dep_vm, repeat=n_s)

		confs = it.product(dep_db, dep_wa, dep_s)
		confs = [tuple(it.chain.from_iterable(t)) for t in
				[it.chain.from_iterable(t) for t in confs]]

		dconfs = list(confs)
		n_dconfs = len(dconfs)
		# print(n_dconfs)
		# print(dconfs[0])
		# sys.exit(0)

		# db_combs = [ () for _ in range(n_db*n_dbms)]
		# n_dbcombs = 0
		# for i in range(n_db):
		# 	for j in range(n_dbms):
		# 		#db_combs[n_dbcombs] = (f'DB{i}', f'DBMS{j}')
		# 		db_combs[n_dbcombs] = (i, j)
		# 		n_dbcombs += 1
		# print(n_dbcombs)
		# # print(f'DB,DBMS COMBINATIONS\n{db_combs}')
		# wa_combs = [ () for _ in range(n_wa*n_ws)]
		# n_wacombs = 0
		# for i in range(n_wa):
		# 	for j in range(n_ws):
		# 		#wa_combs[n_wacombs] = (f'WA{i}', f'WS{j}')
		# 		wa_combs[n_wacombs] = (i, j)
		# 		n_wacombs += 1
		# print(n_wacombs)
		# # print(f'WA,WS COMBINATIONS\n{wa_combs}')
		# s_combs = [ () for _ in range(n_s)]
		# n_scombs = n_s
		# for i in range(n_s):
		# 	#s_combs[i] = (f'S{i}', )
		# 	s_combs[i] = i
		# print(n_scombs)
		# # print(f'S COMBINATIONS\n{s_combs}')
		# vm_combs = [ () for _ in range(n_vm*n_csp)]
		# n_vmcombs = 0
		# for i in range(n_vm):
		# 	for j in range(n_csp):
		# 		#vm_combs[n_vmcombs] = (f'VM{i}', f'CSP{j}')
		# 		vm_combs[n_vmcombs] = (i, j)
		# 		n_vmcombs += 1 
		# # print(n_vmcombs)
		# # print(f'VM,CSP COMBINATIONS\n{vm_combs}')

		# # sys.exit(0)
		# db_dcombs = [ () for _ in range(n_dbcombs*n_vmcombs) ]
		# n_db_dep = 0
		# for i in db_combs:
		# 	for j in vm_combs:
		# 		db_dcombs[n_db_dep] = tuple(i + j)
		# 		#db_dcombs[n_db_dep] = i + j
		# 		n_db_dep += 1
		# print(n_db_dep)
		# # print(f'DB,DBMS,VM,CSP COMBINATIONS\n{db_dcombs}')
		# wa_dcombs = [ () for _ in range(n_wacombs*n_vmcombs) ]
		# n_wa_dep = 0
		# for i in wa_combs:
		# 	for j in vm_combs:
		# 		wa_dcombs[n_wa_dep] = tuple(i + j)
		# 		#wa_dcombs[n_wa_dep] = i + j
		# 		n_wa_dep += 1
		# print(n_wa_dep)
		# # print(f'WA,WS,VM,CSP COMBINATIONS\n{wa_dcombs}')
		# s_dcombs = [ () for _ in range(n_scombs*n_vmcombs) ]
		# n_s_dep = 0
		# for i in s_combs:
		# 	for j in vm_combs:
		# 		s_dcombs[n_s_dep] = tuple((i, ) + j)
		# 		#s_dcombs[n_s_dep] = i + j
		# 		n_s_dep += 1
		# print(n_s_dep)
		# # print(f'S,VM,CSP COMBINATIONS\n{s_dcombs}')

		# # sys.exit(0)

		# # dconfs = [ () for _ in range(n_db_dep*n_wa_dep*n_s_dep) ]
		# dconfs = []
		# n_dconfs = 0
		# for i in range(int(n_db_dep / n_db)):
		# 	# tmp_db_dep = ()
		# 	# tmp_db_dep = tuple([ db_dcombs[j*4+i] for j in range(n_db) ])
		# 	# tmp_db_dep = tuple(db_dcombs[i])
		# 	# print(tmp_db_dep)
		# 	for h in range(int(n_db_dep / n_db)):
		# 		# tmp_db_dep = tuple([ db_dcombs[j*int(n_db_dep/n_db)+h] for j in range(n_db) ])
		# 		tmp_db_dep = (db_dcombs[i])
		# 		# print(f'TDD: {tmp_db_dep}')
		# 		for j in range(n_db):
		# 			tmp_db_dep += tuple(db_dcombs[j*int(n_db_dep/n_db)+h])
		# 			# print(f'TDD: {tmp_db_dep}')
		# 			# for h in range(int(n_db_dep / n_db)):
		# 			# 	tmp_db_dep = tuple([ db_dcombs[i+j*(int(n_db_dep/n_db)+h)] for j in range(n_db) ])
		# 			for q in range(int(n_wa_dep / n_wa)):
		# 				# tmp_wa_dep = ()
		# 				# tmp_wa_dep = tuple([ wa_dcombs[h*4+j] for h in range(n_wa) ])
		# 				for s in range(int(n_wa_dep / n_wa)):
		# 					tmp_wa_dep = (wa_dcombs[q])
		# 					# print(f'TWD: {tmp_wa_dep}')
		# 					for t in range(n_wa):
		# 						tmp_wa_dep += tuple(wa_dcombs[t*int(n_wa_dep/n_wa)+s])
		# 						# print(f'TWD: {tmp_wa_dep}')
		# 						for k in range(int(n_s_dep / n_s)):
		# 							tmp_s_dep = ()
		# 							for t in range(n_s):
		# 								tmp_s_dep += s_dcombs[t*3+k]
		# 								# print(f'TSD: {tmp_s_dep}')
		# 								#dconfs[n_dconfs] = tuple(db_dcombs[i] + wa_dcombs[j] + s_dcombs[k])
		# 								# tmp_ddb = tuple([ g for sub in tmp_db_dep for g in sub ])
		# 								# tmp_dwa = tuple([ g for sub in tmp_wa_dep for g in sub ])
		# 								# tmp_ds = tuple([ g for sub in tmp_s_dep for g in sub ])
		# 								#dconfs[n_dconfs] = tuple(tmp_ddb + tmp_dwa + tmp_ds)
		# 								# print(tuple(tmp_ddb + tmp_dwa + tmp_ds))
		# 								# dconfs.append(tuple(tmp_ddb + tmp_dwa + tmp_ds))
		# 								# print(f'CONF: {tmp_db_dep + tmp_wa_dep + tmp_s_dep}')
		# 								dconfs.append(tmp_db_dep + tmp_wa_dep + tmp_s_dep)
		# 								n_dconfs += 1
		print("Total of possible configurations: {}".format(n_dconfs))
		n_av_configs = 0
		n_del_conf = 0
		# sys.exit(0)
		# print("ALL COMBINATIONS")
		# [ print( f'{dconfs[i]}\n') for i in range(n_dconfs) ]

		# del db_dcombs, wa_dcombs, s_dcombs, db_combs, wa_combs, s_combs
		# del n_dbcombs, n_wacombs, n_vmcombs, n_scombs
		# sys.exit(0)
		data_dconfs = []
		for i, _ in zip(range(n_dconfs), progressbar(range(n_dconfs))):
			# if i == 101:
			# 	sys.exit(0)
			# print("NEW CONFIGURATION CREATION")
			# print(dconfs[i])
			data = {}
			dcomps = 0
			
			data['components'] = []
			data['components'].append({})
			data['components'][0]['nodes'] = []
			used_vms = []
				
			for k in range(n_db) :
				data['components'][0]['nodes'].append({
					'id_node': k + 1,
					'type': "DB",
					'host' : "DBMS{}".format(dconfs[i][k*3+1] + 1), # which DBMS hosts DB component
					'vm' : "VM{}".format(dconfs[i][k*3+2] + 1)	   # which VM hosts DBMS component
				})
				if ("VM{}".format(dconfs[i][k*3+2] + 1) not in used_vms) :
					used_vms.append("VM{}".format(dconfs[i][k*3+2] + 1))
			for k in range(n_wa) :
				data['components'][0]['nodes'].append({
					'id_node': n_db + k + 1,
					'type': "WebApp",
					'host' : "WS{}".format(dconfs[i][n_db*3+k*3+1] + 1), # which WS hosts WA component
					'vm' : "VM{}".format(dconfs[i][n_db*3+k*3+2] + 1)	# which VM hosts WS component
				})
				if ("VM{}".format(dconfs[i][n_db*3+k*3+2] + 1) not in used_vms) :
					used_vms.append("VM{}".format(dconfs[i][n_db*3+k*3+2] + 1))
			for k in range(n_s) :
				data['components'][0]['nodes'].append({
					'id_node': n_db + n_wa + k + 1,
					'type': "S",
					'host' : "VM{}".format(dconfs[i][n_db*3+n_wa*3+k*2] + 1) # which VM hosts S component
				})
				if ("VM{}".format(dconfs[i][n_db*3+n_wa*3+k*2] + 1) not in used_vms) :
					used_vms.append("VM{}".format(dconfs[i][n_db*3+n_wa*3+k*2] + 1))
			
			# print("used_vms: {}".format(used_vms))
			# print(f'conf n.{i} with used vms equal to {len(used_vms)}')
			
			dep_dbms = []
			dep_vm = []
			dep_csp = []
			dep_vm_csp = []
			for j in range(n_db):
				#if dconfs[i][j*4+1] not in dep_dbms:
				dep_dbms.append(dconfs[i][j*3])
				if dconfs[i][j*3+1] not in dep_vm:
					dep_vm.append(dconfs[i][j*3+1])
				if dconfs[i][j*3+2] not in dep_csp:
					dep_csp.append(dconfs[i][j*3+2])
				if (dconfs[i][j*3+1], dconfs[i][j*3+2]) not in dep_vm_csp:
					dep_vm_csp.append((dconfs[i][j*3+1], dconfs[i][j*3+2]))
			# print(f'DEP DBMS: {dep_dbms}')
			dep_ws = []
			for j in range(n_wa):
				#if dconfs[i][n_db*4+j*4+1] not in dep_ws:
				dep_ws.append(dconfs[i][n_db*3+j*3])
				if dconfs[i][n_db*3+j*3+1] not in dep_vm:
					dep_vm.append(dconfs[i][n_db*3+j*3+1])
				if dconfs[i][n_db*3+j*3+2] not in dep_csp:
					dep_csp.append(dconfs[i][n_db*3+j*3+2])
				if (dconfs[i][n_db*3+j*3+1], dconfs[i][n_db*3+j*3+2]) not in dep_vm_csp:
					dep_vm_csp.append((dconfs[i][n_db*3+j*3+1], dconfs[i][n_db*3+j*3+2]))
			# print(f'DEP WS: {dep_ws}')
			for j in range(n_s):
				if dconfs[i][n_db*3+n_wa*3+j*2] not in dep_vm:
					dep_vm.append(dconfs[i][n_db*3+n_wa*3+j*2])
				if dconfs[i][n_db*3+n_wa*3+j*2+1] not in dep_csp:
					dep_csp.append(dconfs[i][n_db*3+n_wa*3+j*2+1])
				if (dconfs[i][n_db*3+n_wa*3+j*2], dconfs[i][n_db*3+n_wa*3+j*2+1]) not in dep_vm_csp:
					dep_vm_csp.append((dconfs[i][n_db*3+n_wa*3+j*2], dconfs[i][n_db*3+n_wa*3+j*2+1]))
			# print(f'DEP VM: {dep_vm}')
			# print(f'DEP CSP: {dep_csp}')
			# print(f'DEP VM-CSP: {dep_vm_csp}')

			#sys.exit(0)
			if (len(dep_csp) > len(dep_vm) or len(dep_vm_csp) > comps or len(dep_vm_csp) > len(dep_vm) or \
				len(dep_vm) < vm_min or len(used_vms) < vm_min) :
				# print("CONFIGURATION REJECTED")
				# print(json.dumps(data, indent=4, sort_keys=True))
				del data
				n_del_conf += 1
				continue
			else :
				# print("NEW CONFIGURATION CREATION")
				data['id'] = n_av_configs + 1
				n_av_configs += 1
			
			data['dcomponents'] = []
			data['dcomponents'].append({})
			data['dcomponents'][0]['nodes'] = []

			for j in range(len(dep_dbms)):
				data['dcomponents'][0]['nodes'].append({})
				data['dcomponents'][0]['nodes'][dcomps]['id_comp'] = dcomps + 1
				data['dcomponents'][0]['nodes'][dcomps]['type'] = "DBMS"
				data['dcomponents'][0]['nodes'][dcomps]['name'] = "DBMS{}".format(dep_dbms[j] + 1)
				data['dcomponents'][0]['nodes'][dcomps]['vendor'] = conf_gen['dbms'][dconfs[i][j*3]]['vendor']
				data['dcomponents'][0]['nodes'][dcomps]['product'] = conf_gen['dbms'][dconfs[i][j*3]]['product']
				data['dcomponents'][0]['nodes'][dcomps]['version'] = conf_gen['dbms'][dconfs[i][j*3]]['version']
				data['dcomponents'][0]['nodes'][dcomps]['host'] = "VM{}".format(dconfs[i][j*3+1] + 1)
				dcomps += 1

			for j in range(len(dep_ws)) :
				data['dcomponents'][0]['nodes'].append({})
				data['dcomponents'][0]['nodes'][dcomps]['id_comp'] = dcomps + 1
				data['dcomponents'][0]['nodes'][dcomps]['type'] = "WS"
				data['dcomponents'][0]['nodes'][dcomps]['name'] = "WS{}".format(dep_ws[j] + 1)
				data['dcomponents'][0]['nodes'][dcomps]['vendor'] = conf_gen['ws'][dconfs[i][n_db*3+j*3]]['vendor']
				data['dcomponents'][0]['nodes'][dcomps]['product'] = conf_gen['ws'][dconfs[i][n_db*3+j*3]]['product']
				data['dcomponents'][0]['nodes'][dcomps]['version'] = conf_gen['ws'][dconfs[i][n_db*3+j*3]]['version']
				data['dcomponents'][0]['nodes'][dcomps]['host'] = "VM{}".format(dconfs[i][n_db*3+j*3+1] + 1)
				dcomps += 1

			for j in range(len(dep_vm)) :
				data['dcomponents'][0]['nodes'].append({})
				data['dcomponents'][0]['nodes'][dcomps]['id_comp'] = dcomps + 1
				data['dcomponents'][0]['nodes'][dcomps]['type'] = "VM"
				data['dcomponents'][0]['nodes'][dcomps]['name'] = "VM{}".format(dep_vm[j] + 1)
				data['dcomponents'][0]['nodes'][dcomps]['vm_type'] = conf_gen['vm'][j]['type']
				data['dcomponents'][0]['nodes'][dcomps]['vendor'] = conf_gen['vm'][j]['vendor']
				data['dcomponents'][0]['nodes'][dcomps]['product'] = conf_gen['vm'][j]['product']
				data['dcomponents'][0]['nodes'][dcomps]['version'] = conf_gen['vm'][j]['version']
				for (h, g) in dep_vm_csp:
					if "VM{}".format(h + 1) == data['dcomponents'][0]['nodes'][dcomps]['name']:
						data['dcomponents'][0]['nodes'][dcomps]['provider'] = "CSP{}".format(g + 1)
				dcomps += 1
			
			for j in range(len(dep_csp)) :
				data['dcomponents'][0]['nodes'].append({})
				data['dcomponents'][0]['nodes'][dcomps]['id_comp'] = dcomps + 1
				data['dcomponents'][0]['nodes'][dcomps]['type'] = "CSP"
				data['dcomponents'][0]['nodes'][dcomps]['name'] = "CSP{}".format(dep_csp[j] + 1)
				data['dcomponents'][0]['nodes'][dcomps]['vendor'] = conf_gen['csp'][j]['vendor']
				data['dcomponents'][0]['nodes'][dcomps]['product'] = conf_gen['csp'][j]['product']
				data['dcomponents'][0]['nodes'][dcomps]['version'] = conf_gen['csp'][j]['version']
				dcomps += 1

			data['num_dcomponents'] = dcomps
			
			data_dconfs.append(data)
			# print(json.dumps(data, indent=4, sort_keys=True))
			# self.write_conf(data)
		
		return (n_av_configs, n_del_conf, data_dconfs)


if __name__ == "__main__" :
	n_db = int(input("Enter number of DB components: "))
	n_wa = int(input("Enter number of WebApp components: "))
	n_s = int(input("Enter number of S components: "))
	vm_min = int(input("Enter number of minimum VM to deploy: "))
	#generate(sys.argv[1]) # argv: number of confs to generate
	cg = ConfigurationGenerator()
	(n_configs, n_del_configs, data) = cg.apply(n_db, n_wa, n_s, vm_min)
	print("Total of available configurations: {}".format(n_configs))
	print("Total of discarded configurations: {}".format(n_del_configs))
