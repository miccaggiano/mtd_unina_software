import numpy as np
import random, math
import json
import seaborn as sns
import matplotlib.pyplot as plt
import effects

from pyahp import parse
from deap import base, creator, tools, algorithms

AC_file = './ahp/ac.json'
DC_file = './ahp/dc.json'
EFF_file = './ahp/effects.json'


def prompt_input(sec_l):
	N = int(input("Enter number of considered attack types: "))
	M = int(input("Enter number of active moving target defenses: "))
	num_mut = sec_l
	#num_mut = sec_l * math.floor(N/M) 
	return (N, M, num_mut)

def normalize(eff_value):
	return ((eff_value - 13) / (39 - 13))

def print_stats(stats):
	sns.set()

	_ = plt.scatter(range(1, len(stats)+1), [ s['mu'] for s in stats ], marker='.')
	_ = plt.title('average fitness per iteration')
	_ = plt.xlabel('iterations')
	_ = plt.ylabel('fitness')
	plt.show()

# define AHP function
def ahp(filename):
	with open(filename) as json_model:
		model = json.load(json_model)
		
		ahp_model = parse(model)
		priorities = ahp_model.get_priorities()
	
	return priorities


def evaluate(sec_l, NM, att_probs, att_awareness=None):
	#(N, M, num_mut) = prompt_input(sec_l)
	N = 13 # attacks
	M = 12 # mutations
	num_mut = int(sec_l)
	# print("N: {}".format(N))
	# print("M: {}".format(M))
	#print("num_mut: {}".format(num_mut))
	DC = ahp(DC_file)
	# print("DC: {}".format(DC))
	AC = ahp(AC_file)
	# print("AC: {}".format(AC))
	(EFF, att_dam) = effects.evaluateEff(EFF_file) # EFF : on rows MTD, on cols ATT
	AM = [ att_dam[i][1] for i in range(N) ]
	# print("EFF: {}".format(EFF))
	# print("AM: {}".format(AM))
	pa = att_probs
	if att_awareness is None :
		AA = [ 0 for _ in range(N)]
	else :
		if len(att_awareness) == N and sum(att_awareness) > 0:
			AA = att_awareness

	def myFunc(individual):
		if sum(AA) > 0 :
			return (sum([ AC[i]*AA[i] for i in range(N) ])	\
					- sum([ DC[i]*individual[i] for i in range(M) ])	\
					+ sum([ sum([ (1 - normalize(sum(EFF[j])))*individual[j] for j in range(M) ])*AM[i]*AA[i] for i in range(N) ])),
		else :
			return (sum([ AC[i] for i in range(N) ])	\
					- sum([ DC[i]*individual[i] for i in range(M) ]) \
					+ sum([ sum([ (1 - normalize(sum(EFF[j])))*individual[j] for j in range(M) ])*AM[i]*pa[i] for i in range(N) ])),


	def feasible(individual):
		"""Feasibility function for the individual. Returns True if feasible False otherwise."""
		if sum(individual) > 0 and sum(individual) <= num_mut :
			if ((individual[6] or individual[7] or individual[8]) and individual[11]) or \
				((individual[6] or individual[7] or individual[8] or individual[11]) and individual[9]):
				return False
			else :
				return True
		else :
			return False

	
	def set_fitness(population):
		fitnesses = [ (individual, tbx.evaluate(individual)) for individual in population ]
		#print(fitnesses)
		for (individual, fitness) in fitnesses:
			#print(fitness[0])
			individual.fitness.values = ( fitness[0], )
	
	def pull_stats(population, iteration=1):
		fitnesses = [ individual.fitness.values[0] for individual in population ]
		#print(fitnesses)
		return {
		    'i': iteration,
		    'mu': np.mean(fitnesses),
		    'std': np.std(fitnesses),
		    'max': np.max(fitnesses),
		    'min': np.min(fitnesses)
		}
	
	creator.create("FitnessMax", base.Fitness, weights=(1.0,))
	creator.create("Individual", list, fitness=creator.FitnessMax)

	tbx = base.Toolbox()

	#INDIVIDUAL_SIZE = 3
	INDIVIDUAL_SIZE = M

	tbx.register("attr_int", random.randint, 0, 1)
	#tbx.register("attr_int", num_mut, 0, M)
	tbx.register("individual", 
		         tools.initRepeat, 
		         creator.Individual,
		         tbx.attr_int, 
		         n=INDIVIDUAL_SIZE)

	tbx.register("population", tools.initRepeat, list, tbx.individual, n=10)

	tbx.register("evaluate", myFunc)
	tbx.decorate("evaluate", tools.DeltaPenalty(feasible, -2))

	#tbx.register("mate", tools.cxOnePoint)
	tbx.register("mate", tools.cxUniform, indpb=0.8) #0.4
	tbx.register("mutate", tools.mutFlipBit, indpb=0.01) #0.5
	tbx.register("select", tools.selTournament, tournsize=3)

	## create random population,
	population = tbx.population()

	## set fitness,
	set_fitness(population)

	## quick look at the initial population,
	population[:5]

	## globals,
	stats = []
	
	iteration = 1
	while iteration < 101:
		#print("Generation : ", iteration)
		current_population = list(map(tbx.clone, population))
		#print(current_population)
		
		offspring = []
		for _ in range(10):
			i1, i2 = np.random.choice(range(len(population)), size=2, replace=False)
			
			offspring1, offspring2 = tbx.mate(population[i1], population[i2])
			
			offspring.append(tbx.mutate(offspring1)[0])
			offspring.append(tbx.mutate(offspring2)[0])
		
		for child in offspring:
			current_population.append(child)
		
		## reset fitness
		set_fitness(current_population)
		#set_fitness(current_population, nc)
		
		population[:] = tbx.select(current_population, len(population))
		
		## set fitness on individuals in the population,
		stats.append(pull_stats(population, iteration))
		
		# print the best result in the current iteration.
		#best_ind = tools.selBest(population, 1)[0]
		#print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values[0]))
		#b_fit = np.max([ individual.fitness.values[0] for individual in current_population ])
		#print("Best partial result : ", b_fit)
		
		iteration += 1
	
	best_ind = tools.selBest(population, 1)[0]
	print("******************")
	#print("Best overall individual is %s" % best_ind)
	#return best_ind

	#print_stats(stats)

	return best_ind


if __name__ == "__main__":
	#DC = ahp(DC_file)
	best_ind = evaluate(9, 1)
	print("Best overall individual is %s" % best_ind)