import json, secrets
import time
import conf_manager
from threading import Lock
import active_conf as ac
# from active_conf import activeConf
import arbiter as arb

green = lambda text: '\033[0;32m' + text + '\033[0m'
yellow = lambda text: '\033[0;33m' + text + '\033[0m'
bold = lambda text: '\033[0;1m' + text + '\033[0m'
basepath = './docs/'

counter = 0
counter_low = 0

csp_dict =	{
    "amazon_aws": {
        "vendor": "amazon_aws_project",
		"product": "amazon_aws",
		"version": "*"
	},
	"cloud_platform": {
		"vendor": "google",
		"product": "cloud_platform",
		"version": "*"
	},
	"azure_devops_server": {
		"vendor": "microsoft",
		"product": "azure_devops_server",
		"version": [ "2019", "2019.0.1" ]
	}
}

os_dict =	{
    "windows_server_2016": {
        "vendor": "microsoft",
		"product": "windows_server_2016",
		"version": ["1709", "1803", "1903"]
	},
	"enterprise_linux": {
        "vendor": "redhat",
        "product": "enterprise_linux",
        "version": ["7.3", "7.5", "8.0", "6.0"]
    },
    "freebsd": {
        "vendor": "freebsd",
        "product": "freebsd",
        "version": ["10.3", "7.3", "9.2", "6.0"]
    },
    "solaris": {
        "vendor": "oracle",
        "product": "solaris",
        "version": ["9", "10"]
    }
}

dbms_dict =	{
    "sqlite": {
        "vendor": "sqlite",
        "product": "sqlite",
        "version": ["3.8.2", "3.25.02", "3.7.17"]
    },
    # "mysql": {
    #     "vendor": "oracle",
    #     "product": "mysql",
    #     "version": ["5.7.19", "8.0.3", "5.7.10"]
    # },
    "postgresql": {
        "vendor": "postgresql",
        "product": "postgresql",
        "version": ["9.4.7", "10.0", "9.6.1"]
    },
    "mariadb": {
        "vendor": "mariadb",
        "product": "mariadb",
        "version": ["10.1.9", "10.1.13", "10.0.2"]
    }
}

ws_dict =	{
    "tomcat": {
        "vendor": "apache",
        "product": "tomcat",
        "version": ["7.0.27", "8.5.14", "7.0.70"]
    },
    "nginx": {
        "vendor": "nginx",
        "product": "nginx",
        "version": ["1.5.2", "1.11.13"]
    },
    "internet_information_server": {
        "vendor": "microsoft",
        "product": "internet_information_server",
        "version": ["7.5", "5.1", "6.0"]
    }
}

defense_durations = {
    "token": 2, "ip": 2, "port": 2, "topology": 7, "mac": 3, "proxy": 4, 
    "web_server": 4, "operating_system": 5, "database": 5,
    "software_diversity": 5, "lm_vm": 11, "service_version": 4
}

class EnforcementUnit(object):


    def __init__(self):
        self.lock = Lock()
        self.activeConf = ac.activeConf
        self.listOS = list(os_dict.keys())
        self.listCSP = list(csp_dict.keys())
        self.listDBMS = list(dbms_dict.keys())
        self.listWS = list(ws_dict.keys())
    

    def increment_counter(self):
        global counter
        counter += 1
        if counter == 6 :
            counter = 0
    

    def increment_counter_low(self):
        global counter_low
        counter_low += 1
        if counter_low == 5 :
            counter_low = 0


    def ss_deploy(self, data):
        # conf_toWrite_id = self.activeConf.id
        # with open(basepath + 'conf_{}.json'.format(conf_toWrite_id)) as f:
        #     data = json.load(f)
        
        n = self.activeConf.num_dcomponents

        # assign IPv4 addresses
        secretsGenerator = secrets.SystemRandom()
        for i in range(n):
            if self.activeConf.dcomponents[i].type == "VM" :
                ip = "192.168.1.{}".format(secretsGenerator.randrange(2, 255))
                # self.activeConf.dcomponents[i].ip_addr = ip
                ac.set_ip(i, ip)
                data['dcomponents'][0]['nodes'][i]['ipv4'] = self.activeConf.dcomponents[i].ip_addr
                print(yellow("IPv4 address of application deployment component {} set to {}".format(self.activeConf.dcomponents[i].name, ip)))

        self.lock.acquire()
        # with open('active_conf.json', 'w') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
            json.dump(data, f)
        self.lock.release()


    def changeIP(self, ip):
        time.sleep(defense_durations.get("ip"))
        all_ips = [ "192.168.1.{}".format(i) for i in range(2, 255) ]
        n = self.activeConf.num_dcomponents
        conf = self.activeConf

        # generate and assign new IPv4 addresses
        secretsGenerator = secrets.SystemRandom()
        ips = []
        for i in range(n):
            if conf.dcomponents[i].type == "VM" :
                ips.append(conf.dcomponents[i].ip_addr)
        
        available_ips = list(set(all_ips) - set(ips))
        new_ip = secretsGenerator.choice(available_ips)

        idx = 0
        for i in range(n):
            if self.activeConf.dcomponents[i].ip_addr == ip :
                idx = i
                self.activeConf.dcomponents[i].ip_addr = new_ip
                print(green("Application deployment component {} successfully changed IPv4 address to {}".format(self.activeConf.dcomponents[i].name, new_ip)))

        # self.lock.acquire()
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
            data = json.load(f)
        data['dcomponents'][0]['nodes'][idx]['ipv4'] = self.activeConf.dcomponents[idx].ip_addr
        self.lock.acquire()
        # with open('active_conf.json', 'w') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
            json.dump(data, f)
        self.lock.release()
    

    def changeIPs(self):
        n = self.activeConf.num_dcomponents

        for i in range(n):
            if self.activeConf.dcomponents[i].ip_addr is not None :
                print(green(f'Attempt to change IP {self.activeConf.dcomponents[i].ip_addr}'))
                self.changeIP(self.activeConf.dcomponents[i].ip_addr)

    
    def changeCSP(self, node):
        list_csp = ["aws", "gcp", "azure"]
        n = self.activeConf.num_dcomponents
        conf = self.activeConf

        # generate and assign new IPv4 addresses
        secretsGenerator = secrets.SystemRandom()
        new_csp = secretsGenerator.choice(list_csp)
        idx = 0
        for i in range(n):
            if conf.dcomponents[i].name == node :
                idx = i
                new_vendor = csp_dict.get(new_csp).get('vendor')
                new_product = csp_dict.get(new_csp).get('product')
                if type(csp_dict.get(new_csp).get('version')) is list :
                    new_version = secretsGenerator.choice(csp_dict.get(new_csp).get('version'))
                else :
                    new_version = csp_dict.get(new_csp).get('version')
        
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
            data = json.load(f)
        data['dcomponents'][0]['nodes'][idx]['vendor'] = new_vendor
        data['dcomponents'][0]['nodes'][idx]['product'] = new_product
        data['dcomponents'][0]['nodes'][idx]['version'] = new_version
        self.lock.acquire()
        # with open('active_conf.json', 'w') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
            json.dump(data, f)
        self.lock.release()
    

    def changeVM(self, id_vm):
        time.sleep(defense_durations.get("lm_vm"))
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
            conf = json.load(f)
        # conf = data
        n = conf['num_dcomponents']
        
        lm_vm = None
        vms = []
        for i in range(n):
            if conf['dcomponents'][0]['nodes'][i]['type'] == "VM" and conf['dcomponents'][0]['nodes'][i]['id_comp'] != id_vm :
                vms.append(conf['dcomponents'][0]['nodes'][i]['name'])
            elif conf['dcomponents'][0]['nodes'][i]['id_comp'] == id_vm :
                lm_vm = conf['dcomponents'][0]['nodes'][i]['name']
                ex_csp = conf['dcomponents'][0]['nodes'][i]['provider']

        secretsGenerator = secrets.SystemRandom()
        new_vm = secretsGenerator.choice(vms)
        
        n_comps = len(conf['components'][0]['nodes'])
        for i in range(n_comps):
            if conf['components'][0]['nodes'][i]['type'] == "S" :
                if conf['components'][0]['nodes'][i]['host'] == lm_vm :
                    conf['components'][0]['nodes'][i]['host'] = new_vm
                    self.activeConf.components[i].host = new_vm
            else :
                if conf['components'][0]['nodes'][i]['vm'] == lm_vm :
                    conf['components'][0]['nodes'][i]['vm'] = new_vm
                    self.activeConf.components[i].vm = new_vm
        
        for i in range(n):
            if conf['dcomponents'][0]['nodes'][i]['type'] == "DBMS" :
                if conf['dcomponents'][0]['nodes'][i]['host'] == lm_vm :
                    conf['dcomponents'][0]['nodes'][i]['host'] = new_vm
                    self.activeConf.dcomponents[i].host = new_vm
            if conf['dcomponents'][0]['nodes'][i]['type'] ==  "WS" :
                if conf['dcomponents'][0]['nodes'][i]['host'] == lm_vm :
                    conf['dcomponents'][0]['nodes'][i]['host'] = new_vm
                    self.activeConf.dcomponents[i].host = new_vm

        idx = -1
        noVMonExCSP = True
        exCSP_idx = None
        for i in range(n):
            if conf['dcomponents'][0]['nodes'][i]['name'] == lm_vm :
                idx = i
            elif (conf['dcomponents'][0]['nodes'][i]['type'] == "VM" and conf['dcomponents'][0]['nodes'][i]['provider'] == ex_csp):
                noVMonExCSP = False
            if (noVMonExCSP and conf['dcomponents'][0]['nodes'][i]['name'] == ex_csp):
                exCSP_idx = i
            
        del conf['dcomponents'][0]['nodes'][idx]    
        del self.activeConf.dcomponents[idx]
        if noVMonExCSP :
            del conf['dcomponents'][0]['nodes'][exCSP_idx - 1]    
            del self.activeConf.dcomponents[exCSP_idx - 1]
            conf['num_dcomponents'] = n - 2
            self.activeConf.num_dcomponents = n - 2
        else :
            conf['num_dcomponents'] = n - 1
            self.activeConf.num_dcomponents = n - 1
        
        self.lock.acquire()
        # with open('active_conf.json', 'w') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
            json.dump(conf, f)
        self.lock.release()
        
        print(green("Application deployment component {} on n.{} successfully migrated to {}".format(lm_vm, id_vm, new_vm)))
        if noVMonExCSP :
            print(green("{} does not provide any host in active configuration".format(ex_csp)))
    

    def changeVMrand(self):
        n = self.activeConf.num_dcomponents

        id_vms = []
        for i in range(n):
            #print(conf.dcomponents[i].type)
            if self.activeConf.dcomponents[i].type == "VM" :
                id_vms.append(self.activeConf.dcomponents[i].id)
        
        if len(id_vms) > 1 :
            secretsGenerator = secrets.SystemRandom()
            ex_vm = secretsGenerator.choice(id_vms)
            print(green(f'Attempt to migrate VM on application deployment component n.{ex_vm}'))
            self.changeVM(ex_vm)
        else :
            print(yellow("+++++++++ MIGRATION NOT ALLOWED +++++++++"))
    

    def changeOS(self, ex_node):
        time.sleep(defense_durations.get("operating_system"))
        n = self.activeConf.num_dcomponents

        for i in range(n):
            if self.activeConf.dcomponents[i].id == ex_node :
                idx = i
                ex_os = self.activeConf.dcomponents[i].product
                node_name = self.activeConf.dcomponents[i].name
        
        #listOS = ["windows_server_2016", "ubuntu_linux", "enterprise_linux", "freebsd"]
        av_os = []
        for os in self.listOS:
            if os != ex_os:
                av_os.append(os)
        #print(av_os)
        secretsGenerator = secrets.SystemRandom()
        new_os = secretsGenerator.choice(av_os)
        
        new_vendor = os_dict.get(new_os).get('vendor')
        self.activeConf.dcomponents[idx].vendor = new_vendor
        new_product = os_dict.get(new_os).get('product')
        self.activeConf.dcomponents[idx].product = new_product
        new_version = os_dict.get(new_os).get('version')

        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
            conf = json.load(f)
        conf['dcomponents'][0]['nodes'][idx]['vendor'] = new_vendor
        conf['dcomponents'][0]['nodes'][idx]['product'] = new_product
        if len(new_version) > 1:
            nv = secretsGenerator.choice(new_version)
            conf['dcomponents'][0]['nodes'][idx]['version'] = nv
            self.activeConf.dcomponents[idx].version = nv
        else:
            conf['dcomponents'][0]['nodes'][idx]['version'] = new_version
            self.activeConf.dcomponents[idx].version = new_version
        
        print(green("Application deployment component {} on component n.{} with {} successfully changed to {}".format(node_name, ex_node, ex_os, new_os)))
        
        self.lock.acquire()
        # with open('active_conf.json', 'w') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
            json.dump(conf, f)
        self.lock.release()
    

    def changeOSrand(self):
        n = self.activeConf.num_dcomponents

        vms = []
        for i in range(n):
            if self.activeConf.dcomponents[i].type == "VM" :
                vms.append(self.activeConf.dcomponents[i].id)
        
        secretsGenerator = secrets.SystemRandom()
        idx_vm = secretsGenerator.choice(vms)
        for i in range(n):
            if self.activeConf.dcomponents[i].id == idx_vm :
                sel_vm = self.activeConf.dcomponents[i].name
        
        print(green(f'Attempt to change OS on {sel_vm}'))
        self.changeOS(idx_vm)
        

    def changeSV(self, ex_node):
        time.sleep(defense_durations.get("service_version"))
        n = self.activeConf.num_dcomponents

        for i in range(n):
            if self.activeConf.dcomponents[i].id == ex_node :
                idx = i
                ex_sv = self.activeConf.dcomponents[i].version
                ex_prod = self.activeConf.dcomponents[i].product
                node_type = self.activeConf.dcomponents[i].type
                node_name = self.activeConf.dcomponents[i].name
        
        warning = False
        secretsGenerator = secrets.SystemRandom()
        if node_type == "VM":
            av_version = os_dict.get(ex_prod).get('version')
        elif node_type == "DBMS":
            av_version = dbms_dict.get(ex_prod).get('version')
        elif node_type == "WS":
            av_version = ws_dict.get(ex_prod).get('version')
        elif node_type == "CSP":
            av_version = csp_dict.get(ex_prod).get('version')

        if len(av_version) > 1:
            new_version = secretsGenerator.choice(av_version)
        elif av_version == ex_sv:
            warning = True

        if warning is False:
            self.activeConf.dcomponents[idx].version = new_version
            print(green("Application deployment component {} on component n.{} with {} successfully changed to {}".format(node_name, ex_node, ex_prod, new_version)))

            # self.lock.acquire()
            # with open('active_conf.json') as f:
            with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
                conf = json.load(f)
            conf['dcomponents'][0]['nodes'][idx]['version'] = new_version
            self.lock.acquire()
            # with open('active_conf.json', 'w') as f:
            with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
                json.dump(conf, f)
            self.lock.release()
        else:
            print(yellow("+++++++++ CHANGING SERVICE VERSION NOT ALLOWED +++++++++"))
    

    def changeSVrand(self):
        n= self.activeConf.num_dcomponents

        types = ["DBMS", "WS", "VM"]
        secretsGenerator = secrets.SystemRandom()
        idx_type = secretsGenerator.choice(types)
        idxs = []
        
        for i in range(n):
            if self.activeConf.dcomponents[i].type == idx_type :
                idxs.append(self.activeConf.dcomponents[i].id)
        
        sel_node = secretsGenerator.choice(idxs)
        # sel_id = conf['dcomponents'][0]['nodes'][sel_node]['id_comp']
        for i in range(n):
            if self.activeConf.dcomponents[i].id == sel_node :
                sel_name = self.activeConf.dcomponents[i].name
                sel_prod = self.activeConf.dcomponents[i].product
        
        print(green(f'Attempt to change service version of {sel_prod} on {sel_name}'))
        self.changeSV(sel_node)
    

    def changeWS(self, ex_node):
        time.sleep(defense_durations.get("web_server"))
        # n = conf['num_dcomponents']
        n = self.activeConf.num_dcomponents
        # conf = self.activeConf

        for i in range(n):
            if self.activeConf.dcomponents[i].id == ex_node :
                idx = i
                ex_ws = self.activeConf.dcomponents[i].product
                node_name = self.activeConf.dcomponents[i].name
        
        #listWS = ["tomcat", "nginx", "internet_information_server"]
        av_ws = []
        for ws in self.listWS:
            if (ws != ex_ws) :
                av_ws.append(ws)
        #print(av_ws)
        secretsGenerator = secrets.SystemRandom()
        new_ws = secretsGenerator.choice(av_ws)
        
        new_vendor = ws_dict.get(new_ws).get('vendor')
        self.activeConf.dcomponents[idx].vendor = new_vendor
        new_product = ws_dict.get(new_ws).get('product')
        self.activeConf.dcomponents[idx].product = new_product
        new_version = ws_dict.get(new_ws).get('version')

        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
            conf = json.load(f)
        conf['dcomponents'][0]['nodes'][idx]['vendor'] = new_vendor
        conf['dcomponents'][0]['nodes'][idx]['product'] = new_product
        if len(new_version) > 1:
            nv = secretsGenerator.choice(new_version)
            conf['dcomponents'][0]['nodes'][idx]['version'] = nv
            self.activeConf.dcomponents[idx].version = nv
        else:
            conf['dcomponents'][0]['nodes'][idx]['version'] = new_version
            self.activeConf.dcomponents[idx].version = new_version
        
        print(green("Application deployment component {} on component n.{} with {} successfully changed to {}".format(node_name, ex_node, ex_ws, new_ws)))
        
        self.lock.acquire()
        # with open('active_conf.json', 'w') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
            json.dump(conf, f)
        self.lock.release()
    

    def changeWSrand(self):
        n = self.activeConf.num_dcomponents

        wss = []
        for i in range(n):
            if self.activeConf.dcomponents[i].type == "WS" :
                wss.append(self.activeConf.dcomponents[i].id)
        
        secretsGenerator = secrets.SystemRandom()
        idx_ws = secretsGenerator.choice(wss)
        for i in range(n):
            if self.activeConf.dcomponents[i].id == idx_ws :
                sel_ws = self.activeConf.dcomponents[i].name
        print(green(f'Attempt to change WS on {sel_ws}'))
        self.changeWS(idx_ws)
    

    def changeDBMS(self, ex_node):
        time.sleep(defense_durations.get("database"))
        n = self.activeConf.num_dcomponents

        for i in range(n):          
            if self.activeConf.dcomponents[i].id == ex_node :
                idx = i
                ex_dbms = self.activeConf.dcomponents[i].product
                node_name = self.activeConf.dcomponents[i].name
        
        #listDBMS = ["tomcat", "nginx", "internet_information_server"]
        av_dbms = []
        for dbms in self.listDBMS :
            if (dbms != ex_dbms) :
                av_dbms.append(dbms)
        #print(av_dbms)
        secretsGenerator = secrets.SystemRandom()
        new_dbms = secretsGenerator.choice(av_dbms)
        
        new_vendor = dbms_dict.get(new_dbms).get('vendor')
        self.activeConf.dcomponents[idx].vendor = new_vendor
        new_product = dbms_dict.get(new_dbms).get('product')
        self.activeConf.dcomponents[idx].product = new_product
        new_version = dbms_dict.get(new_dbms).get('version')
        defnv = None
        if len(new_version) > 1:
            nv = secretsGenerator.choice(new_version)
            defnv = nv
            self.activeConf.dcomponents[idx].version = nv
        else:
            defnv = new_version
            self.activeConf.dcomponents[idx].version = new_version
        
        # self.lock.acquire()
        # with open('active_conf.json') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json') as f:
            conf = json.load(f)
        
        conf['dcomponents'][0]['nodes'][idx]['product'] = new_product
        conf['dcomponents'][0]['nodes'][idx]['vendor'] = new_vendor
        conf['dcomponents'][0]['nodes'][idx]['version'] = defnv
        
        self.lock.acquire()
        # with open('active_conf.json', 'w') as f:
        with open(f'active_conf_{str(arb.ID_TEST)}.json', 'w') as f:
            json.dump(conf, f)
        self.lock.release()
        
        print(green("Application deployment component {} on {} with {} successfully changed to {}".format(node_name, ex_node, ex_dbms, new_dbms)))
    

    def changeDBMSrand(self):
        n = self.activeConf.num_dcomponents

        dbmss = []
        for i in range(n):
            if self.activeConf.dcomponents[i].type == "DBMS" :
                dbmss.append(self.activeConf.dcomponents[i].id)
        
        secretsGenerator = secrets.SystemRandom()
        idx_dbms = secretsGenerator.choice(dbmss)
        for i in range(n):
            if self.activeConf.dcomponents[i].id == idx_dbms :
                sel_dbms = self.activeConf.dcomponents[i].name
        print(green(f'Attempt to change DBMS on {sel_dbms}'))
        self.changeDBMS(idx_dbms)
    

    def changeSD(self, ex_node):
        time.sleep(defense_durations.get("software_diversity"))
        n = self.activeConf.num_dcomponents

        for i in range(n):
            if self.activeConf.dcomponents[i].id == ex_node :
                node_type = self.activeConf.dcomponents[i].type
        
        if node_type == "VM":
            self.changeOS(ex_node)
        elif node_type == "DBMS":
            self.changeDBMS(ex_node)
        elif node_type == "WS":
            self.changeWS(ex_node)
        else:
            print(yellow("+++++++++ CHANGING SOFTWARE NOT ALLOWED +++++++++"))


    def changeSDrand(self):
        conf = self.activeConf
        n = conf.num_dcomponents

        types = ["DBMS", "WS", "VM"]
        secretsGenerator = secrets.SystemRandom()
        idx_type = secretsGenerator.choice(types)
        idxs = []
        
        for i in range(n):
            if conf.dcomponents[i].type == idx_type :
                idxs.append(conf.dcomponents[i].id)
        
        sel_node = secretsGenerator.choice(idxs)
        
        if idx_type == "VM":
            self.changeOS(sel_node)
        elif idx_type == "DBMS":
            self.changeDBMS(sel_node)
        elif idx_type == "WS":
            self.changeWS(sel_node)
        else:
            print(yellow("+++++++++ CHANGING SOFTWARE NOT ALLOWED +++++++++"))