import conf_manager as cm
import enforcement_unit as eu
import conf_generator as cg
import arbiter as arb
import attacker as att
import proactive_unit as pru
from active_conf import activeConf

import random, queue, time, sys
from scipy.stats import uniform
import numpy as np
import itertools as it
from datetime import datetime

colouredRed = lambda text: '\033[0;37;41m' + text + '\033[0m'

attack_types = [
    "ddos", "csrf", "redirect", "xss", "sql_injection", "os_injection", 
    "trojan", "worm", "rootkit", "buffer_overflow", "jamming", "side_channel"
]

# NUM_RANDOM_ATTACKS = 250
# NUM_DETECTED_ATTACKS = 200

BUF_SIZE = 50
att_det_queue = queue.Queue(BUF_SIZE)


def run_MTDFramework():
    if len(sys.argv) == 1 :
        n_db = int(input("Enter number of DB components: "))
        n_wa = int(input("Enter number of WebApp components: "))
        n_s = int(input("Enter number of S components: "))
        # n_s = int(input("A caval donato non si guarda in bocca: "))
        vm_min = int(input("Enter number of minimum VM to deploy: "))
        sl = int(input("Enter security level (1: low, 2: medium, 3: high): "))
        budget = int(input("Enter your budget: "))
        # sys.exit(0)
        arb.set_id_test(0)
        arb.set_number_attacks(288)
        arb.set_perc_det(0.5)
        arb.set_number_detected_attacks()
        arb.set_number_random_attacks()
        att.set_attacker_skills("LOW")
    elif len(sys.argv) == 6 :
        # python run.py id_test num_att perc_det exp_att exp_def
        id_test = sys.argv[1]
        num_att = sys.argv[2]
        perc_det = sys.argv[3]
        exp_att = sys.argv[4]
        exp_def = sys.argv[5]
        print(colouredRed(f'Ongoing test n.{int(id_test)} - NUM_ATT: {int(num_att)}, PERC_DET: {float(perc_det)}, EXP_ATT: {str(exp_att)}, EXP_DEF: {int(exp_def)}'))
        # sys.exit(0)
        n_db = 2
        n_wa = 2
        n_s = 1
        vm_min = 3
        sl = int(exp_def)
        budget = 20

        startTime = datetime.now()

        arb.set_id_test(int(id_test))
        arb.set_number_attacks(int(num_att))
        arb.set_perc_det(float(perc_det))
        arb.set_number_detected_attacks()
        arb.set_number_random_attacks()
        att.set_attacker_skills(str(exp_att))
    else :
        print("ERROR")
        sys.exit(0)
    
    confGen = cg.ConfigurationGenerator()
    (n_configs, n_del_configs, data_dconfs) = confGen.apply(n_db, n_wa, n_s, vm_min)
    print("\nTotal of available configurations: {}".format(n_configs))
    print("Total of discarded configurations: {}".format(n_del_configs))

    confM = cm.ConfigurationManager(budget)
    
    # warm-up
    activeConf = confM.run_setupStage(data_dconfs, n_configs)

    # deploy setup configuration into cloud
    enf_unit = eu.EnforcementUnit()
    enf_unit.activeConf = activeConf
    enf_unit.ss_deploy(data_dconfs[activeConf.id])

    confM.print_active()
    # sys.exit(0)

    proactiveU = pru.ProactiveUnit(confM, enf_unit, sl)
    
    arbiter = arb.Arbiter(sl)
    arbiter.am_unit = proactiveU.am_unit
    
    # proactiveU.start()

    # Prepare attackers
    nra = arb.NUM_RANDOM_ATTACKS
    # print(nra)
    nda = arb.NUM_DETECTED_ATTACKS
    # print(nda)

    threads_att = []
    threads_det = []
    for i, j in it.zip_longest(range(nra), range(nda)):
        # print(i, j)
        # c_at = random.choice([ attack_types[h] for h in range(len(attack_types)) if (int(nra / len(attack_types)) - scheduled_attacks[h]) > 0 ])
        if j is not None :
            if j > 0 :
                if arb.PERC_DET == .25 :
                    attackerDet = att.Attacker(nra + j + 1, 25, 1, arbiter)
                elif arb.PERC_DET == .5 :
                    attackerDet = att.Attacker(nra + j + 1, threads_det[j - 1].time_sleep + random.randrange(30, 51), 1, arbiter)
            else :
                attackerDet = att.Attacker(nra + j + 1, 5, 1, arbiter)
            threads_det.append(attackerDet)
        if i is not None :
            if i > 0 :
                if arb.PERC_DET == .25 :
                    if i < nda :
                        attacker = att.Attacker(i + 1, random.randrange(10, 16), 0, arbiter)
                    else :
                        attacker = att.Attacker(i + 1, threads_att[i-1].time_sleep + random.randrange(5, 16), 0, arbiter)
                elif arb.PERC_DET == .5 :
                    attacker = att.Attacker(i + 1, threads_att[i-1].time_sleep + random.randrange(50, 71), 0, arbiter)
                elif arb.PERC_DET == .0 :
                     attacker = att.Attacker(i + 1, threads_att[i-1].time_sleep + random.randrange(10, 41), 0, arbiter)
            else :
                if arb.PERC_DET == .0 :
                    attacker = att.Attacker(i + 1, random.randrange(5, 11), 0, arbiter)
                else :
                    attacker = att.Attacker(i + 1, 10, 0, arbiter)
            threads_att.append(attacker)
            if arb.PERC_DET == .0 :
                attacker.start()
    
    proactiveU.start()

    if arb.PERC_DET == .25 :
        for attacker, attackerDet in it.zip_longest(threads_att, threads_det):
            if attacker is not None :
                attacker.start()
            if attackerDet is not None :
                attackerDet.start()
                attackerDet.join()
        for attacker in threads_att:
            attacker.join()
    elif arb.PERC_DET == .0 :
        for attacker, attackerDet in it.zip_longest(threads_att, threads_det):
            attacker.join()
    elif arb.PERC_DET == .5 :
        for attacker, attackerDet in it.zip_longest(threads_att, threads_det):
            attackerDet.start()
            attacker.start()
        for attacker, attackerDet in it.zip_longest(threads_att, threads_det):
            attacker.join()
            attackerDet.join()
    
    proactiveU.stop()
    proactiveU.join()

    time.sleep(2)

    endTime = datetime.now()
    durTime = endTime - startTime
    minutes = (durTime.seconds % 3600) // 60
    seconds = durTime.seconds % 60
    arbiter.print_effProbs(minutes, seconds, att.ATTACKER_SKILLS, sl)


if __name__ == "__main__":
    run_MTDFramework()
