from pycvesearch import CVESearch
from attacker import cve_targets
import json

cve_search = CVESearch()
data = {}

for cve in cve_targets:
    print(cve)

    try:
        res = cve_search.id(cve)
        n_vc = len(res['vulnerable_product'])
        vuln_confs = [ res['vulnerable_product'][j] for j in range(n_vc) ]
        data[cve] = vuln_confs
    except:
        print(f'{cve} --> failed to lookup')

with open('vulnConfs_static.json', 'w') as f:
    json.dump(data, f)